<%@include file="/apps/htk/global.jsp" %><%
%><%@ page import="com.day.cq.commons.Doctype,
                   org.apache.commons.lang3.StringEscapeUtils" %><%
    String favIcon = currentDesign.getPath() + "/images/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }
%><head>
    <meta charset="utf-8" />
    <meta http-equiv="keywords" content="<%= StringEscapeUtils.escapeHtml4(WCMUtils.getKeywords(currentPage, false)) %>" />
    <meta http-equiv="description" content="<%= StringEscapeUtils.escapeHtml4(properties.get("jcr:description", "")) %>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <cq:includeClientLib categories="pml.public.css" />
    <cq:includeClientLib categories="htk.public.css" />

    <cq:includeClientLib categories="pml.public.js" />
    
    <cq:include script="/apps/htk/init.jsp" />

    <% if (favIcon != null) { %>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>" />
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>" />
    <% } %>
    <title><%= currentPage.getTitle() == null ? StringEscapeUtils.escapeHtml4(currentPage.getName()) : StringEscapeUtils.escapeHtml4(currentPage.getTitle()) %></title>
    <!-- Maxymiser script start -->
    <script type="text/javascript" src="//service.maxymiser.net/cdn/pennmutual/js/mmcore.js"></script>
    <!-- Maxymiser script end -->
</head>



