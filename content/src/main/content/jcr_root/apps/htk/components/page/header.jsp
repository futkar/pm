<%@
page    session="false"
        import="com.day.cq.commons.Doctype,
                com.pennmutual.utilities.PageUtilities" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%@
taglib  prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %>

<div class="header" id="header">
    <div class="wrapper">
        <a href="<%=PageUtilities.GetUrl(currentPage.getAbsoluteParent(1)) %>"><img id="htk-logo" src="<%=currentDesign.getPath() %>/images/logo.png" alt="Horner Townsend and Kent" height="90" width="95" /></a>

        <cq:include script="search.jsp" />
                
        <p class="off-screen">Global site links:</p>
        
        <cq:include path="global-links" resourceType="pennmutual/public/components/menu/global-links" />



    </div>
</div>