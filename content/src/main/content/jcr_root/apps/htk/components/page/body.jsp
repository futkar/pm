<%@ page
session="false"
import="java.util.ArrayList,
        com.day.cq.wcm.api.WCMMode,
        com.pennmutual.utilities.InheritanceChecker"
%><%@ include file="/apps/pennmutual/public/global.jsp"%><%
    
WCMMode mode =  WCMMode.fromRequest(request);

boolean isPreview = mode == WCMMode.PREVIEW;
boolean isDisabled = mode == WCMMode.DISABLED;
String bodyCssClass = "";
ArrayList<String> bodyCssClasses = new ArrayList<String>();
InheritanceChecker ic = new InheritanceChecker(currentPage);

int showSidebar = ic.getValue("showSidebar");
int showSecondaryMenu = ic.getValue("showSecondaryMenu");

if (showSidebar == 1) {
    bodyCssClasses.add("has-sidebar");
}

if (currentPage.getProperties().get("cq:template", "").contains("blank-page")) {
    bodyCssClasses.add("blank-page");
}

if (bodyCssClasses.size() > 0) {
    StringBuilder sb = new StringBuilder();
    for (String s : bodyCssClasses)
    {
        sb.append(s);
        sb.append(" ");
    }
    bodyCssClass= " class=\"" + sb.toString().trim() + "\"";
}

boolean isBlank = currentPage.getProperties().get("cq:template", "").contains("blank-page");
%>
<body<%=bodyCssClass %>>
    <cq:include path="clientcontext" resourceType="cq/personalization/components/clientcontext"/>

    <% if (!isBlank) { %>
    
    <cq:include script="header.jsp" />
    
    <cq:include path="main-menu" resourceType="pennmutual/public/components/menu/main-menu" />
    
    <%
    if (showSecondaryMenu == 1 || WCMMode.fromRequest(request) == WCMMode.DESIGN) {
        %><cq:include path="secondary-menu" resourceType="pennmutual/public/components/menu/secondary-menu" /><%
    }
    %>

    <% } %>
    
    <cq:include script="main.jsp" />
    
    <cq:include path="footer" resourceType="pennmutual/public/components/footer" />
    
    <% if (!isBlank) { %>   
        <cq:include script="sitemap" />
    <% } %>
    
    <script type="text/javascript">
        ;(function($) {
            $("img.new-window,a.new-window", "body").newWindow();
            
            <% if (isPreview || isDisabled) { %>
            var textSize = "";
            if (window.pmlCookies && window.pmlCookies["text-size"]) {
                switch (window.pmlCookies["text-size"]) {
                    case "big":
                        textSize = "big-text";
                        break;
                    case "small":
                        textSize = "small-text";
                        break;
                    default:
                        break;
                }
                if (textSize) $("body").addClass(textSize);
            }
            <% } %>
        })($PML);
    </script>
    <cq:include path="cloudservices" resourceType="cq/cloudserviceconfigs/components/servicecomponents"/>
</body>