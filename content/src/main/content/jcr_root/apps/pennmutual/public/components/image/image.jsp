<%@ page import="com.day.cq.commons.Doctype,
                     com.day.cq.wcm.api.components.DropTarget,
                     com.day.image.Layer,
                     com.day.cq.wcm.foundation.Image" %><%
%><%@include file="/apps/pennmutual/public/global.jsp"%><%
    Image image = new Image(resource);
    String description = properties.get("jcr:description","");
    int width = 0;
    int height = 0;
    boolean useDimensions = false;
    boolean newWindow = properties.get("newWindow", false);
    
    //drop target css class = dd prefix + name of the drop target in the edit config
    image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
    if (newWindow) image.addCssClass("new-window");
    image.loadStyleData(currentStyle);
    image.setSelector(".img"); // use image script
    image.setDoctype(Doctype.fromRequest(request));
    // add design information if not default (i.e. for reference paras)
    if (!currentDesign.equals(resourceDesign)) {
        image.setSuffix(currentDesign.getId());
    }
    //String imageHeight = "";//image.get(image.getItemName(Image.PN_HEIGHT));
    String divId = "cq-image-jsp-" + resource.getPath();
    if (!description.equals("")) {
        Resource imageResource = resourceResolver.getResource(image.getPath());
        if (imageResource != null) {
            //ImageHelper imageHelper = new ImageHelper();
            //Layer layer = ImageHelper.createLayer(imageResource);
            Layer layer = image.getLayer(true, true, true);
            if (layer != null) {
                width = layer.getWidth();
                height = layer.getHeight();
                useDimensions = true;
            }
        }
    }
    %><div id="<%= divId %>"<% if (!description.equals("")) { %> class="captioned"<% } %><% if (useDimensions) { %> style="width:<%=width%>px"<% } %>><% image.draw(out); %><%
    %><cq:text property="jcr:description" placeholder="" tagName="p" tagClass="caption" escapeXml="true"/>
    </div>
    <%@include file="/libs/foundation/components/image/tracking-js.jsp"%>
