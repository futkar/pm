package com.pennmutual.utilities;

import com.day.cq.wcm.api.Page;
import java.util.ArrayList;
import java.util.Random;

public class PageUtilities {
	
	private static final int RAND_MIN = 100000;
	private static final int RAND_MAX = 999999;
	
	public static String GenerateID(String prefix) {
		Random rand = new Random();
		int randomNum = rand.nextInt(RAND_MAX - RAND_MIN + 1) + RAND_MIN;
		
		prefix = prefix.trim();
		
		if (prefix.isEmpty()) prefix = "id";
		if (!prefix.endsWith("-")) prefix += "-";
		
		return prefix + randomNum;
	}
	
	public static String GetPageTitle(Page page) {
		String title = page.getPageTitle();
		
		if (title == null || title.trim().isEmpty()) title = page.getTitle();
		
		return title;
	}

	public static String GetNavigationTitle(Page page) {
		String title = page.getNavigationTitle();
		
		if (title == null || title.trim().isEmpty()) title = page.getTitle();

		title = title.replaceAll("\\|", "<br />");

		return title;
	}
	public static String GetNavigationTitle(Page page, boolean stripSpecial) {
		String title = page.getNavigationTitle();
		
		if (title == null || title.trim().isEmpty()) title = page.getTitle();
		
		if (stripSpecial) title = title.replace('|', ' ');
		else title = title.replaceAll("\\|", "<br />");
		
		return title;
	}
	
	public static String GetSubnavigationTitle(Page page) {
		String title = page.getProperties().get("subnavTitle", "");
		
		if (title == null || title.trim().isEmpty()) return GetNavigationTitle(page);

		title = title.replaceAll("\\|", "<br />");

		return title;
	}
	public static String GetSubnavigationTitle(Page page, boolean stripSpecial) {
		String title = page.getProperties().get("subnavTitle", "");
		
		if (title == null || title.trim().isEmpty()) return GetNavigationTitle(page, stripSpecial);
		
		if (stripSpecial) title = title.replace('|', ' ');
		else title = title.replaceAll("\\|", "<br />");
		
		return title;
	}
	
	public static String GetAuxNavigationTitle(Page page) {
		String title = page.getProperties().get("auxNavTitle", "");
		
		if (title == null || title.trim().isEmpty()) return GetSubnavigationTitle(page);

		title = title.replaceAll("\\|", "<br />");

		return title;
	}
	public static String GetAuxNavigationTitle(Page page, boolean stripSpecial) {
		String title = page.getProperties().get("auxNavTitle", "");
		
		if (title == null || title.trim().isEmpty()) return GetSubnavigationTitle(page, stripSpecial);
		
		if (stripSpecial) title = title.replace('|', ' ');
		else title = title.replaceAll("\\|", "<br />");
		
		return title;
	}
	
	public static ArrayList<Page> PageChain(Page start) {
		ArrayList<Page> chain = new ArrayList<Page>(start.getDepth());
	
		while (start != null && start.getDepth() > 0) {
			chain.add(start);
			start = start.getParent();
		}
		
		return chain;
	}
	public static ArrayList<String> PathChain(Page start) {
		ArrayList<String> chain = new ArrayList<String>(start.getDepth());
		
		while (start != null && start.getDepth() > 0) {
			chain.add(start.getPath());
			start = start.getParent();
		}
		
		return chain;
	}
	
	public static String GetUrl(Page page) {
		String path = page.getPath();
		
		return GetUrl(path);
	}
	public static String GetUrl(String url) {
		if (url.contains(":")) return url;
		
		if (url.contains("content/dam")) return url;
		
		if (url.endsWith(".html")) return url;
		
		return url + ".html";
	}
	
	public static String BeautifyName(String name) {
		String beautifiedName = name.trim().replaceAll("[^A-Za-z0-9]+", "-").replaceAll("(^-+)|(-+$)", "").toLowerCase();
		
		if (beautifiedName == null || beautifiedName.equals("")){
			beautifiedName = "-";
		}
		
		return beautifiedName;
	}
}