<%
String dailyUnitValuesPath = properties.get("dailyUnitValuesPath", "");
Page duvPage = dailyUnitValuesPath.isEmpty() ? currentPage : pageManager.getPage(dailyUnitValuesPath);
String msg = "Good so far";

if (duvPage != null) {
	String duvPageUrl = PageUtilities.GetUrl(duvPage);
	Resource contentResource = duvPage.getContentResource("par/daily_unitvals");

	if (contentResource != null) {
	   Node contentNode = contentResource.adaptTo(Node.class);
	   if (contentNode != null) {
		   if (contentNode.hasProperty("path")) {
			   String duvDataPath = contentNode.getProperty("path").getValue().getString();   		   
			   Node node = resourceResolver.resolve(duvDataPath).adaptTo(Node.class);
			   if (node != null) {
			       NodeIterator duvProducts = node.getNodes();  
			        
			       while(duvProducts.hasNext()){ 
			            Node duvProductNode = duvProducts.nextNode();
			            if (duvProductNode.getName().equals("jcr:content")) continue;
			            
			            String duvProductTitle = duvProductNode.getProperty("product").getValue().getString();
	
			            %><option value="<%=duvPageUrl%>#<%=PageUtilities.BeautifyName(duvProductTitle)%>"><%=duvProductTitle %></option><%
			       }
			   }
		   }
	   }
	}
} else if (editing || designing) {
%><option>Error: The specified Daily Unit Values page is null.</option><%	
}
%>