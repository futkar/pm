<%@ page
    session="false"
    import="java.util.Date,com.pennmutual.utilities.DateUtilities" %><%@
include
    file="/apps/pennmutual/public/global.jsp"%><%

    Date defaultDate = new Date();
    Date date = pageProperties.get("publishedDate", defaultDate);
    
    String format = properties.get("dateFormat", "MM/dd/yyyy");
%>
<p><span class="news-date"><%=DateUtilities.FormatDate(date, format) %></span></p>
