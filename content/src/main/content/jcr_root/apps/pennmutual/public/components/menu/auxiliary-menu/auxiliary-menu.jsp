<%@ page
session="false"
import="org.apache.commons.lang3.StringUtils,
        org.apache.commons.lang3.StringEscapeUtils,
        com.day.cq.wcm.api.WCMMode,
        com.day.cq.wcm.api.components.DropTarget,
        com.day.cq.wcm.api.PageFilter,
        java.util.Iterator,
        java.util.ArrayList,
        com.pennmutual.utilities.PageUtilities,
        com.pennmutual.utilities.InheritanceChecker"
%><%@ include file="/apps/pennmutual/public/global.jsp"%><%
String style = properties.get("style", "tab");
String scriptName = style + ".jsp";
InheritanceChecker ic = new InheritanceChecker(currentPage);
boolean pageHasSecondaryMenu = ic.getValue("showSecondaryMenu") == 1;
boolean includeHome = properties.get("includeHome", false);

// This component makes a big assumption that it will be defined on
// iparsys's's only.  Perhaps a future mod can be applied to determine
// this behavior on regular parsyss.
//
// The component finds the page that contains the defined component
// and uses that as a basis for generating a list of menu items.
Page startingPage = null;

if (pageHasSecondaryMenu) {
    startingPage = pageManager.getContainingPage(resourceResolver.getResource(currentNode.getPath()));
} else {
    Style secondaryMenuStyle = currentDesign.getStyle(currentPage.getTemplate().getName() + "/secondary-menu");
    int depth = secondaryMenuStyle.get("depth", 3); // Was hardcoded to 3
    
    startingPage = currentPage.getAbsoluteParent(depth);

    if (startingPage == null) {
        // If the condition above fails it means the page is not showing the secondary menu and the depth
        // that describes where the secondary menu is "rooted" is missing or invalid with the value "3".
        // To prevent erroring out, we'll build the auxillary menu based on the page where the menu itself
        // was defined.
        startingPage = pageManager.getContainingPage(resourceResolver.getResource(currentNode.getPath()));
    }
}

Iterator<Page> pages = startingPage.listChildren();

ArrayList<String> chain = PageUtilities.PathChain(currentPage);


boolean noHomeActive = true;
if (includeHome && !startingPage.isHideInNav()) {
    noHomeActive = false;
    while (pages.hasNext()) {
        Page p = pages.next();
        if (chain.contains(p.getPath())) {
            noHomeActive = true;
            break;
        }
    }
    pages = startingPage.listChildren();
}
%>
<ul class="<%=style %>">
    <% if (includeHome && !startingPage.isHideInNav()) { %>
        <% request.setAttribute("menuItem", startingPage); %>
        <li<% if (!noHomeActive && chain.contains(startingPage.getPath())) { %> class="active"<% } %>><cq:include script="<%=scriptName %>" /></li>
    <% } %>
    <%
    
    while (pages.hasNext()) {
        Page p = pages.next();
        if (p.isHideInNav()) continue;
        request.setAttribute("menuItem", p);
        ArrayList<String> cssClasses = new ArrayList<String>(1);
        
        if (chain.contains(p.getPath())) cssClasses.add("active");
        
        boolean special = p.getProperties().get("special", false);
        if (special) cssClasses.add("special");
        
        String cssClass = "";
        
        if (cssClasses.size() > 0) {
            cssClass = " class=\"" + StringUtils.join(cssClasses, " ") + "\"";
        }
    %>
    <li<%=cssClass %>><cq:include script="<%=scriptName %>" /></li>
    <%
    }
    %>
</ul>