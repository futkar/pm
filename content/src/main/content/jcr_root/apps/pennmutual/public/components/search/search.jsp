<%@ page import="com.day.cq.dam.api.Asset,
                 com.day.cq.wcm.foundation.Search,
                 com.day.cq.search.QueryBuilder,
                 com.day.cq.search.Query,
                 com.day.cq.search.PredicateGroup,
                 com.day.cq.search.Predicate,
                 com.day.cq.search.result.SearchResult,
                 com.day.cq.search.result.Hit,
                 com.day.cq.tagging.TagManager,
                 com.pennmutual.utilities.DateUtilities,
                 com.pennmutual.utilities.PageUtilities,
                 org.apache.commons.lang3.StringEscapeUtils,
                 java.lang.Math,
                 java.util.List,
                 java.util.HashMap,
                 java.util.Locale,
                 java.util.Map,
                 java.util.ResourceBundle,
                 com.day.cq.i18n.I18n" %>
<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Search component

  Draws the search form and evaluates a query

--%><%@include file="/apps/pennmutual/public/global.jsp" %><%
%><cq:setContentBundle source="page" /><%
    Search search = new Search(slingRequest);

    final Locale pageLocale = currentPage.getLanguage(true);
    final ResourceBundle resourceBundle = slingRequest.getResourceBundle(pageLocale);
    I18n i18n = new I18n(resourceBundle);

    // Hack to get the values of the local search dictionary (./i18n/en.xml) extracted by xliff
    if (false) {
        i18n.get("nextText", "Please Translate 'Next'");
        i18n.get("noResultsText", "Please Translate 'Your search - &lt;b>{0}&lt;/b> - did not match any documents.'");
        i18n.get("previousText", "Please Translate 'Previous'");
        i18n.get("relatedSearchesText", "Please Translate 'Related searches:'");
        i18n.get("resultPagesText", "Please Translate 'Results'");
        i18n.get("searchButtonText", "Please Translate 'Search'");
        i18n.get("searchTrendsText", "Please Translate 'Search Trends'");
        i18n.get("similarPagesText", "Please Translate 'Similar Pages'");
        i18n.get("spellcheckText", "Please Translate 'Did you mean:'");
        i18n.get("cq/statistics/components/statisticsText", "Please Translate 'Results {0} - {1} of {2} for &lt;b>{3}&lt;/b>. ({4} seconds)'");
    }

    String searchIn = (String) properties.get("searchIn");
    String requestSearchPath = request.getParameter("path");
    if (searchIn != null) {
        // only allow the "path" request parameter to be used if it
        // is within the searchIn path configured
        if (requestSearchPath != null && requestSearchPath.startsWith(searchIn)) {
            search.setSearchIn(requestSearchPath);
        } else {
            search.setSearchIn(searchIn);
        }
    } else if (requestSearchPath != null) {
        search.setSearchIn(requestSearchPath);
    }
   
   
    String escapedQuery = StringEscapeUtils.escapeEcmaScript(search.getQuery());
    pageContext.setAttribute("escapedQuery", escapedQuery);

    log.error("\n\n\n\n***** Search: " + search);
    log.error("***** Search.result is null: " + (search.getResult() == null ? "TRUE" : "FALSE"));
    log.error("***** Search.result: " + search.getResult() + "\n\n\n");
   
    pageContext.setAttribute("search", search);
    TagManager tm = resourceResolver.adaptTo(TagManager.class);
%><c:set var="trends" value="${search.trends}"/><c:set var="result" value="${search.result}"/>

<form action="${currentPage.path}.html">
    <input size="41" maxlength="2048" name="q" value="${fn:escapeXml(search.query)}"/>
    <input value="<fmt:message key="searchButtonText"/>" type="submit"/>
</form>
<br/>

<c:choose>
    <c:when test="${empty result && empty search.query}"></c:when>
    <c:when test="${empty result.hits}">
        ${result.trackerScript}
        <c:if test="${result.spellcheck != null}">
            <p><fmt:message key="spellcheckText"/> <a href="<c:url value="${currentPage.path}.html"><c:param name="q" value="${result.spellcheck}"/></c:url>"><b><c:out value="${result.spellcheck}"/></b></a></p>
        </c:if>
        <fmt:message key="noResultsText">
            <fmt:param value="${fn:escapeXml(search.query)}"/>
        </fmt:message>
        <span record="'noresults', {'keyword': '<c:out value="${escapedQuery}"/>', 'results':'zero', 'executionTime':'${result.executionTime}'}"></span>
    </c:when>
    <c:otherwise>
        <span record="'search', {'keyword': '<c:out value="${escapedQuery}"/>', 'results':'${result.totalMatches}', 'executionTime':'${result.executionTime}'}"></span>
        ${result.trackerScript}
        <p><fmt:message key="cq/statistics/components/statisticsText">
            <fmt:param value="${result.startIndex + 1}"/>
            <fmt:param value="${result.startIndex + fn:length(result.hits)}"/>
            <fmt:param value="${result.totalMatches}"/>
            <fmt:param value="${fn:escapeXml(search.query)}"/>
            <fmt:param value="${result.executionTime}"/>
        </fmt:message></p>


        <c:if test="${fn:length(search.relatedQueries) > 0}">
            <br/><br/>
            <fmt:message key="relatedSearchesText"/>
            <c:forEach var="rq" items="${search.relatedQueries}">
                <a style="margin-right:10px" href="${currentPage.path}.html?q=${rq}"><c:out value="${rq}"/></a>
            </c:forEach>
        </c:if>
        <br/>
        <c:forEach var="hit" items="${result.hits}" varStatus="status">
            <c:if test="${hit.extension != \"\" && hit.extension != \"html\"}">
                <span class="icon type_${hit.extension}"><img src="/etc/designs/default/0.gif" alt="*"></span>
            </c:if>
            <h4><a href="${hit.URL}" onclick="trackSelectedResult(this, ${status.index + 1})">${hit.title}</a></h4>
            <p class="excerpt"><span class="off-screen">Excerpt: </span>${hit.excerpt}</p>
            <p class="url">${hit.URL}<c:if test="${!empty hit.properties['cq:lastModified']}"> - <c:catch>Last Modified: <fmt:formatDate value="${hit.properties['cq:lastModified'].time}" dateStyle="medium"/></c:catch></c:if> - <a class="similar" href="${hit.similarURL}"><fmt:message key="similarPagesText"/></a></p>
        </c:forEach>
        <c:if test="${fn:length(result.resultPages) > 1}">
            <fmt:message key="resultPagesText"/>
            <c:if test="${result.previousPage != null}">
                <a href="${result.previousPage.URL}"><fmt:message key="previousText"/></a>
            </c:if>
            <c:forEach var="page" items="${result.resultPages}">
                <c:choose>
                    <c:when test="${page.currentPage}">${page.index + 1}</c:when>
                    <c:otherwise>
                        <a href="${page.URL}">${page.index + 1}</a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:if test="${result.nextPage != null}">
                <a href="${result.nextPage.URL}"><fmt:message key="nextText"/></a>
            </c:if>
        </c:if>
    </c:otherwise>
</c:choose>