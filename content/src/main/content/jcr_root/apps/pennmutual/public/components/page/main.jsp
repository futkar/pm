<%@ page
session="false"
import="com.pennmutual.utilities.InheritanceChecker"%><%@
include file="/apps/pennmutual/public/global.jsp"
%><%
InheritanceChecker ic = new InheritanceChecker(currentPage);
int showSidebar = ic.getValue("showSidebar");
int showPageUtilities = ic.getValue("showPageUtilities");
%>
    <div class="main" id="main">
        <div class="wrapper">
            <% if (showPageUtilities == 1) {%><cq:include script="page-utilities.jsp" /><% } %>
            <cq:include script="content-area.jsp" />
            <% if (showSidebar == 1) {%><cq:include script="right-sidebar.jsp" /><% } %>
        </div>
    </div>