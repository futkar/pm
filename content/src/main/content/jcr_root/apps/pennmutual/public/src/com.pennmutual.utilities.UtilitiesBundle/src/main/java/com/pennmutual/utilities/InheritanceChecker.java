package com.pennmutual.utilities;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.ValueMap;

public class InheritanceChecker {
	private Page _start;
	public InheritanceChecker(Page page) {
		_start = page;
	}
	public int getValue(String propertyName) {
		return getValue(propertyName, _start);
	}
	
	public int getValue(String propertyName, Page page) {
		if (page == null) return 0;
		
		ValueMap properties = page.getProperties();
		int value = properties.get(propertyName, -1); 
		
		if (value == -1) {
			return getValue(propertyName, page.getParent());
		} else {
			return value;
		}
	}
	
}