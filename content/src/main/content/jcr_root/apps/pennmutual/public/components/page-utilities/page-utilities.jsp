<%@include file="/apps/pennmutual/public/global.jsp"%><%
String designPath = currentDesign.getPath();
%>
<div class="page-utilities">
    <ul>
        <li><a href="#"><img src="<%=designPath %>/images/icon-text-size.png" alt="Change Text Size" width="20" height="16" /></a></li>
        <li><a href="#"><img src="<%=designPath %>/images/icon-print.png" alt="Print" width="16" height="16" /></a></li>
        <li><a href="#"><img src="<%=designPath %>/images/icon-rss.png" alt="RSS feed" width="16" height="16" /></a></li>
    </ul>
</div>