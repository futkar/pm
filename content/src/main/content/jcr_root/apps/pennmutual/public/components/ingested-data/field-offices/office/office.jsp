<%@include file="/apps/pennmutual/public/global.jsp"%><%
String region = currentNode.getNode("regional-office").getProperty("fieldValue").getString();
String address = currentNode.getNode("address").getProperty("fieldValue").getString();
String city = currentNode.getNode("city").getProperty("fieldValue").getString();
String state = currentNode.getNode("state").getProperty("fieldValue").getString();
String zip = currentNode.getNode("zip").getProperty("fieldValue").getString();
String telephone = currentNode.getNode("telephone").getProperty("fieldValue").getString();
String name = currentNode.getNode("field-or-second-line-manager").getProperty("fieldValue").getString();
%>
    <h3><%= region%></h3>
    <p>
        <span class="address"><%=address %></span><br />
        <span class="city"><%=city %></span>, <span class="state"><%=state %></span>, <span class="zip"><%=zip %></span><br />
        <span class="telephone"><%=telephone %></span><br />
        <span class="bold">Contact Name:</span> <span class="contact-name"><%=name %></span>
    </p>