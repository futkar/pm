<%@
page import="
            java.util.ArrayList,
            java.util.Collections,
            java.util.Hashtable,
            java.util.SortedSet,
            java.util.Iterator,
            javax.jcr.Node,
            com.day.cq.wcm.api.WCMMode,
            com.day.cq.wcm.api.components.IncludeOptions,
            com.pennmutual.utilities.StateHelper,
            com.pennmutual.utilities.StateHelper.State,
            com.pennmutual.utilities.PageUtilities"%><%@
include file="/apps/pennmutual/public/global.jsp"%><%
WCMMode mode = WCMMode.fromRequest(request);
boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;

String fieldOfficesPath = properties.get("fieldOfficesPath", "");
ArrayList<String> stateNameList = new ArrayList<String>(48);

Hashtable<String,String> states = new Hashtable<String,String>();

StateHelper stateHelper = new StateHelper();

if (!fieldOfficesPath.isEmpty()) {
    Page fieldOfficesPage = pageManager.getPage(fieldOfficesPath);
    if (fieldOfficesPage != null) {
        Resource parResource = fieldOfficesPage.getContentResource("par");
        Iterator<Resource> componentResources = parResource.listChildren();
        
        while (componentResources.hasNext()) {
            Resource componentResource = componentResources.next();
            if (componentResource.isResourceType("pennmutual/public/components/ingested-data/field-offices")) {
                Node fieldOfficesNode = componentResource.adaptTo(Node.class);
                
                if (fieldOfficesNode == null) {
                    break;
                }
                
                if (!fieldOfficesNode.hasProperty("path") || fieldOfficesNode.getProperty("path").getString().isEmpty()) break;
                
                String fieldOfficeDataPath = fieldOfficesNode.getProperty("path").getString();
                Node fieldOfficeDataNode = (Node)resourceResolver.resolve(fieldOfficeDataPath).adaptTo(Node.class);
                NodeIterator stateNodes = fieldOfficeDataNode.getNodes();

                while (stateNodes.hasNext()) {
                    Node stateNode = stateNodes.nextNode();
                    String abbrev = stateNode.getName();
                    
                    if (abbrev.equals("jcr:content")) continue;
                    
                    String stateName = stateHelper.get(abbrev).getName();
                    
                    if (states.containsKey(stateName)) continue;
                    
                    stateNameList.add(stateName);
                    states.put(stateName, abbrev);
                }
                Collections.sort(stateNameList);
                break;
            }
        }

    }
}
%>
<div class="parsys off-screen">
    <form action="<%=PageUtilities.GetUrl(fieldOfficesPath) %>" method="GET">
        <label><span class="off-screen">Select a state:</span>
        <select name="state">
            <option value="">Select a state</option>
            <%
            for (String stateName: stateNameList) {
                
            %>
              <option value="<%=states.get(stateName) %>"><%=stateName %></option>
            <%
            }
            %>
            
        </select>
        </label><br />
        <input type="submit" class="button" value="View" />
    </form>
</div>