<%@page session="false"
            contentType="text/html; charset=utf-8"
            import="com.day.cq.commons.Doctype,
                    com.day.cq.wcm.api.WCMMode,
                    com.day.cq.wcm.foundation.ELEvaluator" %><%@
include file="/apps/pennmutual/public/global.jsp"
%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%
%><cq:defineObjects/><%

%>

<h2>You Are Leaving Our Site – External Link</h2>

<p class="disclaimer">The information being provided is strictly as a courtesy.  When you link to
this web site, you are leaving the Penn Mutual or HTK site.  We make no
representation as to the completeness or accuracy of information provided at
this site.  Nor is the company liable for any direct or indirect technical or
system issues or any consequences arising out of your access to or your use of
third-party technologies, web sites, information and programs made available
through the web site.  When you access this web site, you are leaving Penn Mutual's
or HTK's web site and assume total responsibility and risk for your use of the
web site to which you are linking.<br />
<a class="url">{SHOW LINK}</a>
</p>

<p class="buttons">
    <a class="cancel" href="#">Cancel</a>
    <a class="okay" href="#">Continue</a>
</p>
<div class="x">X</div>

