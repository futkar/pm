<%@
page import="com.day.cq.wcm.api.WCMMode,
             org.apache.sling.commons.json.JSONObject,
             com.pennmutual.utilities.PageUtilities" %><%@
 include file="/apps/pennmutual/public/global.jsp"%><%
String id = properties.get("carouselId", "");
String typ = properties.get("type", "large");
boolean autoRotate = properties.get("autoRotate", false);
long autoRotateInterval = properties.get("autoRotateInterval", 5000);
long autoRotateResumeInterval = properties.get("autoRotateResumeInterval", 10000);
int num = 0;
String[] items = properties.get("items", String[].class);

if (items != null) num = items.length;

if (num > 0) {

JSONObject jsonOptions = new JSONObject();

if (typ.equals("large")) {
%><%@ include file="large.jsp" %><%
} else {
%><%@ include file="small.jsp" %><%
}
} else { %>
    <p>No carousel items defined.</p>
<% } %>