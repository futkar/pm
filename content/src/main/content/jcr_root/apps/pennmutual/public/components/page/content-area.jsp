<%@
page session="false" %><%@
page import="com.day.cq.wcm.api.WCMMode" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode = WCMMode.fromRequest(request);
boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;

String redirectTarget = properties.get("redirectTarget", "");
%>
<div class="content-area">

    <cq:include path="headipar" resourceType="foundation/components/iparsys" />
    <% if (editing || designing) { %>
        <div style="margin:10px 0;padding:5px;text-align:center;background-color:#eeeeee;border:2px dashed white;color:#cccccc;font-weight:bold;font-size:10px;font-family:Tahoma, Arial, Sans-Serif">Inherited paragraph system end</div>
    <% } %>
    <% if ((editing || designing) && !redirectTarget.equals("")) { %>
        <p class="redirect">This page redirects to <a href="<%=redirectTarget%>.html"><%=redirectTarget %></a></p>
    <% } %>
    <cq:include path="par" resourceType="foundation/components/parsys" />
    
</div>