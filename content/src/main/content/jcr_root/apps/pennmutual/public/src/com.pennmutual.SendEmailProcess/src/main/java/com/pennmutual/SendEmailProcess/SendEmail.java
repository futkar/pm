package com.pennmutual.SendEmailProcess;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Value;

import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.EmailException;

/*import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageTemplate;

import com.day.cq.mailer.MailingException;*/

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MailService;

public class SendEmail  {
    
    protected static final Logger log = LoggerFactory.getLogger(SendEmailProcess.class);

    private String message = "";
    private MailService mailService;
    
    public SendEmail(String template, Node payload, MailService mailService) {
    	
    	//log.info("\n****\n\nSending Email\n\n****\n");
    	
    	this.mailService = mailService;
    	
    	ArrayList<String> propertyPlaceholders = DiscoverTemplateProperties(template);
    	message = InterpretTemplate(propertyPlaceholders, template, payload);

    }
    
    private String InterpretTemplate(ArrayList<String> propertyPlaceholders, String template, Node payload) {
    	Pattern pattern = Pattern.compile("\\$\\{([\\w\\.]+)\\}");
    	
    	for (String propertyPlaceholder: propertyPlaceholders) {
    		String propName = "";
    		String propValue = "";
    		
    		Matcher matcher = pattern.matcher(propertyPlaceholder);
    		if (!matcher.matches() || matcher.groupCount() != 1) continue;
    		
    		String matchedProperty = matcher.group(1);
    		
    		if (matchedProperty.startsWith("payload.")) {
    			String[] pieces = matchedProperty.split("\\.");
    			propName = pieces[1];
    			
    			//log.info("\n**** prop = " + propName + " ****\n");
        		
    			try {
    				Property property = payload.getProperty(propName);

    				if (!property.isMultiple()) {
    					propValue = property.getValue().getString();
    				} else {
    					
    					for (Value value: property.getValues()) {
    						propValue += value.getString() + ", ";
    					}
    					
    					if (propValue.endsWith(", ")) propValue = propValue.substring(0, propValue.lastIndexOf(", "));
    				}
					
    			}
    			catch(Exception ex) {
    				propValue = "";
    			}
    			
    			template = template.replace(propertyPlaceholder, propValue);
    		}
    		
    		//log.info("\n****\n\nInterpreted Template:\n=====================\n\n" + template + "\n\n****\n");
    	}
    
    	return template;
    }
    
    private ArrayList<String> DiscoverTemplateProperties(String template) {
    	//log.info("\n****\n\nDiscovering template properties\n\n****\n");
    	ArrayList<String> properties = new ArrayList<String>();
    	Pattern pattern = Pattern.compile("\\$\\{[\\w\\.]+\\}");
    	Matcher matcher = pattern.matcher(template);
    	
    	while (matcher.find()) {
    		//log.info("\n****\n" + matcher.group() + "\n****\n");
    		String match = matcher.group();
    		
    		if (properties.contains(match)) continue;
    		
    		properties.add(match);
    	}
    	return properties;
    }
    
    private SimpleEmail CreateEmail(String message) throws EmailException {
    	if (message == null || message.isEmpty()) throw new IllegalArgumentException("Message content can't be null or empty.");
    	
    	String[] messageLines = message.split("\n\n");
    	String headers = "";
    	
    	if (messageLines.length > 1) {
    		headers = messageLines[0];
    	} else {
    		throw new IllegalArgumentException("Message is either missing the necessary headers or the body of content.  Separate the headers from the body with a single blank line in the email template");
    	}
    	
    	boolean hasTo = false;
    	SimpleEmail email = new SimpleEmail();
    	String[] headerLines = headers.split("\n");
    	
    	for (String header: headerLines) {
    		if (header.startsWith("From:")) {
    			email.setFrom(header.substring(5).trim());
    			
    		} else if (header.startsWith("To:")) {
    			email.addTo(header.substring(3).trim());
    			hasTo = true;
    			
    		} else if (header.startsWith("Subject:")) {
    			email.setSubject(header.substring(8).trim());
    			
    		} else if (header.startsWith("Cc:")) {
    			email.addCc(header.substring(3).trim());
    			
    		} else if (header.startsWith("Bcc:")) {
    			email.addBcc(header.substring(4).trim());
    			
    		}
    	}

    	if (!hasTo) throw new IllegalArgumentException("The message does not contain the To: header.");
    	
    	StringBuilder sb = new StringBuilder();
    	
    	for (int x=1;x<messageLines.length;x++) {
    		sb.append(messageLines[x] + "\n\n");
    	}
    	
    	email.setMsg(sb.toString());
    	
    	return email;
    }
    public void Send() throws Exception {
    	
    	SimpleEmail email = CreateEmail(message);
    	
    	mailService.send(email);
    	
    	//log.info("\n****\n\nEmail sent!\n\n****\n");
    }
}