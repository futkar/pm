<%
String relationship = properties.get("relationship", "children");
String context = properties.get("context", "current");
Boolean markSelected = properties.get("markSelected", false);
Iterator<Page> pages = null;
Page contextPage = null;

if (context.equals("current")) {
    contextPage = currentPage;
} else if (context.equals("path")) {
	String relationshipPath = properties.get("relationshipPath", "");
	contextPage = pageManager.getPage(relationshipPath);
} else {
	contextPage = pageManager.getContainingPage(resource);
}
if (relationship.equals("children")) {
	pages = contextPage.listChildren(new PageFilter());
} else if (relationship.equals("siblings")) {
	if (contextPage.getParent() != null) {
		pages = contextPage.getParent().listChildren(new PageFilter());
	}
}
while (pages.hasNext()) {
	Page aPage = pages.next();
	
    String text = PageUtilities.GetPageTitle(aPage);
    String path = PageUtilities.GetUrl(aPage);
    String selected = currentPage.getPath().equals(aPage.getPath()) ? " selected=\"selected\"" : "";
%>
<option<%=selected %> value="<%=path%>"><%=text %></option>
<%
}
%>