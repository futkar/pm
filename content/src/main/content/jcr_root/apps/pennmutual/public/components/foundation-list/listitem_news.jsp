<%
%><%@ page session="false"
           import="java.text.DateFormat,
                   java.util.Date,
                   com.day.cq.wcm.api.Page,
                   org.apache.commons.lang3.StringEscapeUtils,
                   com.pennmutual.utilities.PageUtilities,
                   com.pennmutual.utilities.DateUtilities"%>
<%@include file="/libs/foundation/global.jsp"%><%

    Page listItem = (Page)request.getAttribute("listitem");
    String title = listItem.getTitle() != null ? listItem.getTitle() : listItem.getName();
    String description = listItem.getDescription() != null ? listItem.getDescription() : "";
    Date defaultDate = new Date();
    //Date date = listItem.getProperties().get("publishedDate", listItem.getProperties().get("cq:lastModified", defaultDate));
    
    Date date = listItem.getProperties().get("publishedDate", defaultDate);
    
    %><li><p><a href="<%= listItem.getPath() %>.html" title="<%= StringEscapeUtils.escapeHtml4(title) %>"
                onclick="CQ_Analytics.record({event: 'listNewsItemClicked', values: { listItemPath: '<%= listItem.getPath() %>' }, collect:  false, options: { obj: this }, componentPath: '<%=resource.getResourceType()%>'})"><%

    %><span class="title"><%= StringEscapeUtils.escapeHtml4(PageUtilities.GetPageTitle(listItem)) %></span></a><br /><%
                
    if (defaultDate.getTime() != date.getTime()) {
    	%><span class="date"><%=DateUtilities.FormatDate(date, "MMMM d, yyyy")%></span><%
    }

    if (!"".equals(description)) {
        %><br/><span class="description"><%= StringEscapeUtils.escapeHtml4(description) %></span><%
    }

%></p></li>