package com.pennmutual.ingestion;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import javax.jcr.Node;
import com.day.text.csv.Csv;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.wcm.api.Page;
import com.day.cq.dam.indd.PageComponent;

public class MonthlyPerformanceIngestor extends CsvIngestor {
	
	public MonthlyPerformanceIngestor(){}
	
	public void extractData(WorkflowData dataItem){
		
		log.info("\n\n****\nMonthlyPerformanceIngestor()\n****\n\n");
		
		try{
			Node dataNode = (Node) this.session.getItem(dataItem.getPayload().toString());
			InputStream ips = dataNode.getProperty("jcr:content/jcr:data").getBinary().getStream();
	
			try{
				Csv csv = new Csv();
				Iterator<String[]> csvIterator = csv.read(ips, null);
				ArrayList<ArrayList<String>> csvFileContents = convertCsvToArray(csvIterator);
				
				log.info("\n\n****\ndatastoreNode = " + (this.datastoreNode == null ? "null" : this.datastoreNode.getPath()) + "\n****\n\n");
				storeData(this.datastoreNode, csvFileContents);
				
				csv.close();
			} catch (Exception e ){}
			ips.close();
		} catch (Exception e){ 
			log.info("Error during data extraction",e); 
		}
		
	}
	
	public void storeData(Node datastore, ArrayList<ArrayList<String>> contents){
		try{
			/*
			 * First row is fields
			 * First column is Fund Category - Will be duplicates, acts as a node
			 * Second Column is fund Name - Will use as 'datanode'
			 * Third column and on will be stored as properties.
			 */
			ArrayList<String> fieldNames = contents.get(0);
			
			//log.info("\n****\nJcrUtil.createPath(" + datastore.getPath() + ")\n****\n\n");
			Node fieldRoot = JcrUtil.createPath(datastore, "fields", false, "nt:unstructured", "nt:unstructured", this.session, true);
			for(int i = 0; i < fieldNames.size(); i++){
				Node field = JcrUtil.createPath(fieldRoot, JcrUtil.createValidName(fieldNames.get(i)), false, "nt:unstructured", "nt:unstructured", this.session, true);
				field.setProperty("fieldName", fieldNames.get(i));
				this.session.save();
			}
			
			boolean gotDate = false;
			for(int i = 1; i < contents.size(); i++){
				Node fundCategory = JcrUtil.createPath(datastore, JcrUtil.createValidName(contents.get(i).get(0)), false, "nt:unstructured", "nt:unstructured", this.session, true);
				Node fund = JcrUtil.createPath(fundCategory, JcrUtil.createValidName(contents.get(i).get(1)), false, "nt:unstructured", "nt:unstructured", this.session, true);
				
				fundCategory.setProperty("categoryName", contents.get(i).get(0));
				fund.setProperty("fundName", contents.get(i).get(1));
				fundCategory.getSession().save();
				fund.getSession().save();
				// Starting at 2 to actually get properties.
				for(int j = 2; j < contents.get(0).size(); j++){
					Node field = JcrUtil.createPath(fund, JcrUtil.createValidName(fieldNames.get(j)), false, "nt:unstructured", "nt:unstructured", this.session, false);
					
					// Save the "Monthly - As of" date to the "asOfDate" property of the fieldRoot
					if (!gotDate && j == 4) {
						fieldRoot.setProperty("asOfDate", contents.get(i).get(j));
						log.info("\n\n****\nRecording as of date: " + contents.get(i).get(j) + "\n****\n\n");
						this.session.save();
						gotDate = true;
					}
					field.setProperty("fieldName", fieldNames.get(j));
					field.setProperty("fieldValue", contents.get(i).get(j));
					field.getSession().save();
					// Start at 2, map properties to field names, set values.
				}
			}
		} catch (Exception e ){
			log.error("Error storing data", e);
		}
	}
	
	public void createPage(String path){
		try{
			
			Page pageRoot = this.pageManager.getPage(path);
			if(pageRoot == null){
				this.builder.createPage(path, "monthly-performance", "Monthly Performance", "/apps/pennmutual/public/templates/monthly-performance", "/etc/designs/pennmutual/public", new ArrayList<PageComponent>());
				// implement updateComponents method
			} else {
				// updateComponents method
			}
			
		} catch (Exception e ){
			log.warn("Error creating page.  Shoot.", e);
		}
	}
}
