<%@
page    import="com.day.cq.wcm.api.WCMMode,
                java.util.Iterator,
                com.day.cq.wcm.api.PageFilter,
                com.pennmutual.utilities.PageUtilities" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode =  WCMMode.fromRequest(request);
boolean isEdit = mode == WCMMode.EDIT;
boolean isDesign = mode == WCMMode.DESIGN;

String selectID = PageUtilities.GenerateID("select");
String submitID = PageUtilities.GenerateID("submit");

String performancePath = slingRequest.getAttribute("performancePath") != null ? (String)slingRequest.getAttribute("performancePath") : "";

if (performancePath.isEmpty()) return;

Page performancePage = resourceResolver.resolve(performancePath).adaptTo(Page.class);

if (performancePage == null) return;

Iterator<Page> pages = performancePage.listChildren(new PageFilter());

if (pages == null || !pages.hasNext()) return;
%>
<h3 class="active"><span>Performance</span></h3>
<form action="#" method="GET">
    <label class="off-screen" for="<%=selectID %>">Select a Product: </label>
    <select id="<%=selectID %>">
           <option value="">Select a product</option>
        <%
        while (pages.hasNext()) {
            Page subpage = pages.next();
        %>
           <option value="<%=PageUtilities.GetUrl(subpage)%>"><%=PageUtilities.GetPageTitle(subpage) %></option>
        <% } %>
    </select><br />
    <label for="<%=submitID %>" class="off-screen">View selected product:</label>
    <input id="<%=submitID %>" class="button" type="submit" value="View" />
</form>