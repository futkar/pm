<%@
page session="false" %><%@
page import="com.pennmutual.utilities.PageUtilities,
             com.day.cq.wcm.api.WCMMode,
             java.util.Set,
             com.day.cq.wcm.api.components.IncludeOptions" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%
String text = properties.get("text", "Button Text");
String href = properties.get("href", "#");
String alignment = properties.get("alignment", "");
boolean flote = properties.get("float", false);
String target = properties.get("newWindow", false) ? " target=\"_blank\"" : "";
String width = properties.get("width", "");

if (!href.contains("://") && !href.contains("#")) href += ".html";
if (flote && alignment.isEmpty()) alignment = "right";
%>
<% if (true == false && !flote) { %><div class="<%=alignment %>"><% } %>
<a<%=target %> href="<%=href %>"><span><%=text %></span></a>
<% if (true == false && !flote) { %></div><% } %>