<%
jsonOptions.put("autoRotate", autoRotate);
jsonOptions.put("autoRotateInterval", autoRotateInterval);
jsonOptions.put("autoRotateResumeInterval", autoRotateResumeInterval);
%>
<div class="items">
<%
int x = 0;
for (String item: items) {
    JSONObject jsonObj = new JSONObject(item);
    String linkUrl = jsonObj.getString("linkUrl");
    String linkCaption = jsonObj.getString("linkCaption");
    String text = jsonObj.getString("text");
    boolean newWindow = jsonObj.getBoolean("openInNewWindow");
    if (!linkUrl.isEmpty() && !linkUrl.contains(":") && !linkUrl.startsWith("/content/dam")) linkUrl += ".html";
%>
    <div class="item<% if (x++ == 0) { %> active<% } %>">
       <img src="<%=jsonObj.getString("image") %>" alt="" />
       <% if (!text.isEmpty()) { %><p><%= jsonObj.getString("text") %><% if (!linkCaption.isEmpty() && !linkUrl.isEmpty()) { %><br /><a<% if (newWindow) { %> target="_blank"<% } %> href="<%=linkUrl %>"><%=linkCaption %> &raquo;</a><% } %></p><% } %>
    </div>
<%
}
%>
</div>
<ul class="nav">
<%
x = 0;
for (String item: items) {
    JSONObject jsonObj = new JSONObject(item);
%>
    <li>
        <p><%= jsonObj.getString("title") %></p>
        <div class="arrow"></div>
    </li>
<%
}
%>
</ul>
<script type="text/javascript">;(function($) { $(document).ready(function() { $('div.<%=id%>').LargeCarousel(<%=jsonOptions.toString()%>); }); })($PML);</script>