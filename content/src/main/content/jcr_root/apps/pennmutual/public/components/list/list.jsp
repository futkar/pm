<%@
page session="false" %><%@
page import="com.pennmutual.utilities.PageUtilities,
             com.day.cq.wcm.api.WCMMode,
             java.util.Set,
             com.day.cq.wcm.api.components.IncludeOptions" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode = WCMMode.fromRequest(request);

boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;
boolean disabled = mode == WCMMode.DISABLED;
boolean readonly = mode == WCMMode.READ_ONLY;

IncludeOptions ic = (new IncludeOptions().getOptions(request, true));

boolean collapsible = properties.get("collapsible", false);
String tag = properties.get("tag", "ul");

if (!tag.equals("ul") && !tag.equals("ol")) tag = "ul";

Set cssClasses = ic.getCssClassNames();

String idCss = "";

if (collapsible) {
	idCss = PageUtilities.GenerateID("list");
	cssClasses.add(idCss);
	
	if (!disabled && !readonly && !preview) {
		cssClasses.add("collapsible");
		cssClasses.add("default-open");
		
	}
}

ic.setDecorationTagName(tag);

%>
<cq:include path="listpar" resourceType="pennmutual/public/components/list/parsys" />
<% if ((disabled || readonly || preview) && collapsible) { %><script type="text/javascript">
;(function($) { $("#main <%=tag%>.<%=idCss%>").collapsible() })($PML);
</script><% } %>