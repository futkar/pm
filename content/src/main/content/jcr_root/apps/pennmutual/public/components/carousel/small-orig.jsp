<%

%>
<div class="items">
<%
int x = 0;
for (String item: items) {
    JSONObject jsonObj = new JSONObject(item);
    String linkUrl = jsonObj.getString("linkUrl");
    String linkCaption = jsonObj.getString("linkCaption");
    String title = jsonObj.getString("title");
    boolean newWindow = jsonObj.getBoolean("openInNewWindow");
    boolean isVideo = jsonObj.getBoolean("isVideo");
    if (!linkUrl.isEmpty() && !linkUrl.contains(":") && !linkUrl.startsWith("/content/dam")) linkUrl += ".html";
%>
    <p class="item"><a href="<%=linkUrl %>"><img<% if (isVideo) {%> class="video"<% } %> src="<%=jsonObj.getString("image") %>" alt="" /><br /><%=title %></a></p>
<%
}
%>
</div>
<script type="text/javascript">;(function($) { $('div.<%=id%>').SmallCarousel(<%=jsonOptions.toString()%>); })($PML);</script>
