package com.pennmutual.ingestion;

import javax.jcr.Session;

import com.day.cq.dam.indd.PageBuilderFactory;
import com.day.cq.dam.indd.PageBuilder;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import com.day.cq.commons.jcr.JcrUtil;

import com.day.cq.wcm.api.PageManagerFactory;
import com.day.cq.wcm.api.PageManager;

import com.day.cq.replication.*;

public abstract class DataIngestor implements Ingestor{
	
	protected static final Logger log = LoggerFactory.getLogger(DataIngestor.class);
	protected ResourceResolver resourceResolver = null;
	protected PageBuilder builder = null;
	protected String datastorePath = null;
	protected String pagestorePath = null;
	protected Node datastoreNode = null;
	protected Session session = null;
	protected PageManager pageManager = null;
	protected Replicator replicator = null;
	
	public void init(ResourceResolverFactory resolverFactory, Replicator replicator, PageBuilderFactory builderFactory, PageManagerFactory pageManagerFactory, WorkflowSession wfSession, String pageStore, String dataStore){
		try{
			this.resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
            this.builder = builderFactory.getPageBuilder(this.resourceResolver);
            this.datastorePath = dataStore;
            this.pagestorePath = pageStore;
            this.session = wfSession.getSession();
            this.pageManager = pageManagerFactory.getPageManager(this.resourceResolver);
            this.replicator = replicator;
            
		} catch (Exception e){
			log.error("Error during init process", e);
		}
	}
	
	public void execute(WorkItem workItem){
		createDataStore(this.datastorePath);
		log.info("\n\n****\nDataIngestor.execute() -- this.datastorePath = " + this.datastorePath + "\n****\n\n");
		extractData(workItem.getWorkflowData()); 
		//createPage(this.pagestorePath);
	}
	
	public void createDataStore(String dataStorePath){
		Node datastore = null;
		try{
			datastore = JcrUtil.createPath(this.datastorePath, "cq:Page", this.session);
			JcrUtil.createUniqueNode(datastore, "jcr:content", "nt:unstructured", this.session);
			
			if(datastore.isNew()){
				this.session.save();
			} else {
				removeDataStore(datastore);
				// Implement versioning here as needed instead of removing first.
				//datastore = JcrUtil.createPath(this.datastorePath, "nt:unstructured", this.session);
				datastore = JcrUtil.createPath(this.datastorePath, "cq:Page", this.session);
				JcrUtil.createUniqueNode(datastore, "jcr:content", "nt:unstructured", this.session);
				
				this.session.save();
			}
		} catch (Exception e){
			log.error("Error creating data store", e);
		}
		
		//log.info("\n\n****\nDataIngestor.createDataStore(dataStorePath = " + dataStorePath + ")\nthis.datastore.getPath() = " + (this.datastore == null ? " <datastore == null> " : this.datastore.getPath()));
		this.datastoreNode = datastore;
	}
	
	protected void removeDataStore(Node datastore){
		try{
			//Node dataParent = datastore.getParent();
			datastore.remove();
			this.session.save();
		} catch (Exception e){
			log.error("Error during data store removal process", e);
		}
	}
	
	protected String beautifyName(String name){
		String beautifiedName = name.replaceAll("[^A-Za-z0-9]+", "-").replaceAll("(^-+)|(-+$)", "").toLowerCase();
		if(beautifiedName == null || beautifiedName.equals("")){
			beautifiedName = "-";
		}
		return beautifiedName;
	}
	
}
