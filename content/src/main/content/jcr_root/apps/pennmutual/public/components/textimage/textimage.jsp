<%@ page import="com.day.cq.commons.Doctype,
                     com.day.cq.wcm.api.WCMMode,
                     com.day.cq.wcm.api.components.DropTarget,
                     com.day.image.Layer,
                     com.day.cq.wcm.foundation.Image" %><%
%><%@include file="/apps/pennmutual/public/global.jsp"%><%
    Image image = new Image(resource, "image");
    String description = image.get("jcr:description");
    int width = 0;
    int height = 0;
    boolean useDimensions = false;
    boolean newWindow = properties.get("newWindow", false);
    
    if (image.hasContent() || WCMMode.fromRequest(request) == WCMMode.EDIT) {
        image.loadStyleData(currentStyle);
        // add design information if not default (i.e. for reference paras)
        if (!currentDesign.equals(resourceDesign)) {
            image.setSuffix(currentDesign.getId());
        }
        //drop target css class = dd prefix + name of the drop target in the edit config
        image.addCssClass(DropTarget.CSS_CLASS_PREFIX + "image");
        if (newWindow) image.addCssClass("new-window");
        image.setSelector(".img");
        image.setDoctype(Doctype.fromRequest(request));

        String divId = "cq-textimage-jsp-" + resource.getPath();

        if (!description.equals("")) {
            Resource imageResource = resourceResolver.getResource(image.getPath());
            if (imageResource != null) {
                Layer layer = image.getLayer(true, true, true);
                if (layer != null) {
                    width = layer.getWidth();
                    height = layer.getHeight();
                    useDimensions = true;
                }
            }
        }
        // div around image for additional formatting
        %><div class="image" id="<%= divId %>"<% if (useDimensions) { %> style="width:<%=width%>px"<% } %>><% image.draw(out); %><%
        %><cq:text property="image/jcr:description" placeholder="" tagClass="caption" tagName="p"/><%
        %></div>
        <%@include file="/libs/foundation/components/image/tracking-js.jsp"%>
        <%
    }
    %><cq:text property="text" tagClass="text"/>