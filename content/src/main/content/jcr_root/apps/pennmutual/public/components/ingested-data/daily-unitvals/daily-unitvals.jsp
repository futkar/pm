<%@include file="/apps/pennmutual/public/global.jsp"%><%@
page import="com.pennmutual.utilities.DateUtilities,
             com.pennmutual.utilities.PageUtilities" %><%
%><%@page import="java.util.Iterator"%><%

    String source = properties.get("path", "/content/ingested-data/unitvals");

    try{
	    
	    Node node = resourceResolver.resolve(source).adaptTo(Node.class);
	    NodeIterator products = node.getNodes();  
	    
	    if (products.hasNext()) {
%>
    <div class="text section">
        <p>The values below are as of close of business on <%=DateUtilities.FormatDate(node.getProperty("jcr:content/jcr:lastModified").getDate(), "MM/dd/yyyy") %>.<br />
        Calculated by Penn Mutual Financial Operations.<br /><br /></p>
    </div>
<%
	    }
	    while(products.hasNext()){ 
	        Node productNode = products.nextNode();
	        
	        if (productNode.getName().equals("jcr:content")) continue;
	        
	        String productTitle = productNode.getProperty("product").getValue().getString();

            NodeIterator fundNodes = productNode.getNodes();
            if (!fundNodes.hasNext()) continue;
%>
			
    <table class="basic" summary="Daily unit values for <%=productTitle %>">
		<caption id="<%=PageUtilities.BeautifyName(productTitle)%>"><%=productTitle.trim() %></caption>
		<thead>
		    <tr>
		        <th scope="col">Fund Name</th>
		        <th scope="col">Unit Values</th>
		    </tr>
		</thead>
		<tbody>
			    <% 
			    while(fundNodes.hasNext()){ 
			    	  Node fundNode = fundNodes.nextNode();
			    	  String fundName = fundNode.getProperty("fundName").getValue().getString();
			    	  String fundValue = fundNode.hasProperty("fundValue") ? fundNode.getProperty("fundValue").getValue().getString() : "";
			    %>
			<tr>
				<td><%=fundName %></td>
				<td><%=fundValue %></td>
			</tr>    
			    <% } %>
        </tbody>
    </table>
    <div class="icon-up button right section">
        <a href="#top"><span>Back to Top</span></a>
    </div>
<%      } 
    } catch (Exception e){
    	%>NO DATA (<%=e.getMessage()%>)<%
    }
    
    %>
    <span record="'dailyUnitViews', {'path':'<%= currentPage.getPath() %>', 'Source':'<%= source %>'}"></span>
    <script type="text/javascript">
        CQ_Analytics.record({event: 'testEvent', values: { testVar1 : 'Hello' }, collect: false, options: { obj:this, defaultLinkType: 'X' }, componentPath: '<%= resource.getResourceType() %>'});
    </script>
