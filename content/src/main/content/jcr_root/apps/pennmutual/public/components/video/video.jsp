<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  HTML5 video component

  ==============================================================================

--%><%@ include file="/apps/pennmutual/public/global.jsp" %><%
%><%@ page import="com.day.cq.dam.api.Asset,
                   com.day.cq.wcm.api.WCMMode, 
                   com.day.cq.analytics.sitecatalyst.Framework,
                   com.day.cq.wcm.webservicesupport.Configuration,
                   com.day.cq.wcm.webservicesupport.ConfigurationManager,
                   com.day.cq.wcm.api.components.DropTarget,
                   org.apache.sling.api.request.RequestParameter" %><%

    boolean popup = request.getParameter("popup") != null && !request.getParameter("popup").isEmpty();         
    boolean strobe = (request.getParameter("strobe") != null && request.getParameter("strobe").equals("true")) || request.getHeader("User-Agent").toLowerCase().indexOf("msie") != -1;
    
    // try find referenced asset
    Asset asset = null;
    Resource assetRes = resourceResolver.getResource(properties.get("asset", ""));
    if (assetRes != null) {
        asset = assetRes.adaptTo(Asset.class);
    }
    String width = "0";
    String height = "0";
    
    if (asset != null) {
        request.setAttribute("video_asset", asset);

       // allow both pixel & percentage values
        width = properties.get("width", currentStyle.get("width", String.class));
        height = properties.get("height", currentStyle.get("height", String.class));
        
        // allow either just a width or a height to be set (letting the browser handle it)
        // but give a default if nothing is set
        if (width == null && height == null) {
            width = "480";
            height = "320";
        }
        String wh = (width != null ? "width=\"" + width + "\"" : "") + " " + (height != null ? "height=\"" + height + "\"" : "");
        StringBuilder attributes = new StringBuilder();

        String videoClass = currentStyle.get("videoClass", "");
        if (videoClass.length() > 0) {
            attributes.append(" class=\"").append(videoClass).append("\"");
        }
        /*if (!currentStyle.get("noControls", false)) {
            attributes.append(" controls=\"controls\"");
        }*/
        if (currentStyle.get("autoplay", false)) {
            attributes.append(" autoplay=\"autoplay\"");
        }
        if (currentStyle.get("loop", false)) {
            attributes.append(" loop=\"loop\"");
        }
        String preload = currentStyle.get("preload", "");
        if (preload.length() > 0) {
            attributes.append(" preload=\"").append(preload).append("\"");
        }
        String id = "cq-video-html5-" + System.currentTimeMillis();
%> 
<%--cq:includeClientLib js="cq.video" /--%>
<%

    if (strobe) {
%>
        <cq:include script="flash.jsp"/>
<%
    } else {
%>
        <video id="<%= id %>" <%= wh %><%= attributes %> >
            <cq:include script="sources.jsp"/>
            <cq:include script="flash.jsp"/>
        </video>
        <div class="off-screen video-controls" style="width: <%=width %>px">
            <div class="button play"><div></div></div>
            <div class="separator"><div></div></div>
            <!--div class="button skip"><div></div></div>
            <div class="separator"><div></div></div-->
            <div class="progress-bar"><div><div></div></div></div>
            <div class="time-elapsed-duration"><div>00:00 / 00:00</div></div>
            <div class="separator"><div></div></div>
            <div class="button full-screen"><div></div></div>
            <div class="separator"><div></div></div>
            <div class="button mute"><div></div></div>
        </div>
        <div class="off-screen watermark"></div>
<%
    
%><% if (!popup) { %>
        <script type="text/javascript">
        ;(function($) {
            $(document).ready(function() {
                var id = '#<%=id%>';
                $(id).Video({
                    mediaName: '<%= asset.getMetadataValue("dc:title")!= "" ? asset.getMetadataValue("dc:title") : asset.getName() %>',
                    mediaFile: '<%= asset.getName() %>',
                    mediaPath: '<%= asset.getPath() %>',
                    resourceType: '<%=resource.getResourceType()%>'
                });
            });
        })($PML);
        </script>
<% } else { %>
    <span style="display:none">
        <var class="mediaName"><%= asset.getMetadataValue("dc:title")!= "" ? asset.getMetadataValue("dc:title") : asset.getName() %></var>
        <var class="mediaFile"><%= asset.getName() %></var>
        <var class="mediaPath"><%= asset.getPath() %></var>
        <var class="resourceType"><%=resource.getResourceType()%></var>
    </span>
<% } %>
<%
    }
        request.removeAttribute("video_asset");
    } else {
%>
<div class="<%= DropTarget.CSS_CLASS_PREFIX + "video" + (WCMMode.fromRequest(request) == WCMMode.EDIT ? " cq-video-placeholder" : "") %>"></div>
<%
    }
%>