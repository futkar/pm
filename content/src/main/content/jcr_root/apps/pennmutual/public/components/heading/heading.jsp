<%@include file="/apps/pennmutual/public/global.jsp"%><%
String tagName = properties.get("tagname", "h2");
String text = properties.get("text", "Heading Text");
String link = properties.get("link", "");
String extension = link.contains("http://") ? "" : ".html";
boolean hasLink = !link.isEmpty();
boolean newWindow = properties.get("newwindow", false);

text = xssAPI.filterHTML(text);
%>
<<%=tagName%>><% if (hasLink) { %><a<% if (newWindow) { %> target="_blank"<% } %> href="<%=link %><%=extension %>"><%
    }%><%=text %><% if (hasLink) { %></a><% } %></<%=tagName%>>