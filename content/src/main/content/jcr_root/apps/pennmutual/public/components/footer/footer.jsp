<%@ page import="com.day.cq.wcm.api.WCMMode"%><%@
include file="/apps/pennmutual/public/global.jsp"%><%
WCMMode mode = WCMMode.fromRequest(request);
boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;
boolean isHomePage = currentPage.getDepth() == 2;
String basePath = !isHomePage ? currentPage.getAbsoluteParent(1).getPath() : currentPage.getPath();
String contentPath = "";
if (!isHomePage) {
    contentPath = basePath + "/jcr:content/footer/content";
}

%>
<hr />
<div class="wrapper">
  <% if(!currentPage.getPath().startsWith("/content/public/asset-management")){ %>
    <%--p>basePath = <%=basePath %><br />currentPage.getDepth() = <%=currentPage.getDepth() %><br />isHomePage = <%=isHomePage %><br />contentPath = <%=contentPath %><br /></br><br /></p--%>
    <% if (isHomePage) { %>
        <cq:include path="content" resourceType="pennmutual/public/components/text" />
    <% } else { %>
        <sling:include path="<%=contentPath %>" />
    <% }
    } %>
</div>