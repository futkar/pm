<%
    final String topMsg = properties.get("topanswermsg",String.class);
    final String boxMsg = properties.get("boxmsg",String.class);
    final String perfectMsg = properties.get("perfectscoremsg", String.class);
    final String[] scoringClasses = properties.get("scoring",String[].class);
    
    int numYes = answersString.toString().replaceAll("[^1]","").length();
    int numNo = answersString.toString().replaceAll("[^0]","").length();
      
    String score = AssessToolUtilities.getScoreFromAnswers(numYes, scoringClasses);
    if (score == null) score = "";
    pageContext.setAttribute("score", score==null ? "<span style=\"color:red;\">error</span>" : score);

    Map<String,Object> tokenValues = new HashMap<String,Object>();
    tokenValues.put("score", score);
    tokenValues.put("numyes",new Integer(numYes));
    tokenValues.put("numquestions", new Integer(numYes+numNo));
    final String topMessage = AssessToolUtilities.applyTokens(tokenValues,topMsg); 
    final String boxMessage = AssessToolUtilities.applyTokens(tokenValues,boxMsg); 
    final String perfectMessage = AssessToolUtilities.applyTokens(tokenValues,perfectMsg);     
       
    pageContext.setAttribute("topMsg", topMessage == null ? "" : topMessage);
    pageContext.setAttribute("boxMsg", boxMessage == null ? "" : boxMessage);
    pageContext.setAttribute("perfectMsg",perfectMessage == null ? "" : perfectMessage);
    
    pageContext.setAttribute("numYes",numYes);
    pageContext.setAttribute("numNo",numNo);
   
    final JSONArray userAnswerArray = new JSONArray(answersString.toString());
    List<String> responseList = AssessToolUtilities.getResponseList(userAnswerArray,  quesAnsArray);
    
    pageContext.setAttribute("responseList",responseList);
 
%>
<c:if test="${empty quesAnsArray}">
    <p>Warning:<br />
    Questions and answers have not been provided. Please provide them by editing the component.</p>
</c:if>
    
<c:if test="${!empty quesAnsArray}">
    <div class="assessment-ans-wrapper">
            <div class="assessment-ans-heading">
                <div class="assessment-ans-box">${boxMsg}</div>
            </div>
        <c:if test="${numNo > 0}">
                <div class="assessment-ans-topmsg">${topMsg}</div>
            <div class="assessment-answers">
            <ul> 
            <c:forEach items="${responseList}" var="resp" varStatus="status">
                 <li>${resp}</li>
            </c:forEach>
                 
            </ul>
            </div>
        </c:if> 

        <c:if test="${numNo == 0 && numYes > 0}">
            ${perfectMsg}
        </c:if>
  
        <p class="assessment-link"><span><a href="${currentPage.path}.html">Click here</a></span> to start over</p>
        <p><input id="${classId}-submit-print" class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" onClick="window.print();return false;" value="Print This Assessment" /></p>
    </div>
</c:if>