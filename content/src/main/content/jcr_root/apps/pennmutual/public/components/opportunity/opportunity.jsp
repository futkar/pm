<%@ page
    session="false"
    import="java.util.Date,
            java.text.SimpleDateFormat,
            com.pennmutual.utilities.PageUtilities,
            com.pennmutual.utilities.DateUtilities" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

String jobTitle = properties.get("jobTitle", PageUtilities.GetPageTitle(currentPage));

Date defaultDate = new Date();
Date postedDate = properties.get("postedDate", defaultDate);

String format = "MMMM d, yyyy";

String  department = properties.get("department", "");
String  location = properties.get("location", "Horsham, PA");
String  category = properties.get("category", "Unknown");
String  positionOverview = properties.get("positionOverview", "");
String  roleResponsibilities = properties.get("roleResponsibilities", "");
String  apply = properties.get("apply", "");
String  qualifications = properties.get("qualifications", "");
String  commonPolicy = properties.get("commonPolicy", "");
Boolean relocation = properties.get("relocation", false);
String  status = properties.get("status", "Not exempt");
String  workHours = properties.get("workHours", "9-5");
String  travel = properties.get("travel", "No travel requirements");
%>
<h1 class="news-title"><%=jobTitle %></h1>

<p>Posted: <%=DateUtilities.FormatDate(postedDate, format) %></p>

<p class="gray-box">
    <strong>Department:</strong> <%=department %><br />
    <strong>Location:</strong> <%=location %><br />
    <strong>Category:</strong> <%=category %><br />
    <strong>Status:</strong> <%=status %>
</p>

<h2>Relocation Available</h2>
<p><% if (relocation) { %>Yes<% } else { %>No<% } %></p>

<h2>Work Hours</h2>
<p><%=workHours %></p>

<h2>Travel Required</h2>
<p><%=travel %></p>

<% if (!positionOverview.equals("")) { %>
    <h2>Position Overview</h2>
    <%=positionOverview %>
<% } %>

<% if (!roleResponsibilities.equals("")) { %>
    <h2>Role Responsibilities</h2>
    <%=roleResponsibilities %>
<% } %>


<% if (!qualifications.equals("")) { %>
    <h2>Qualifications</h2>
    <%=qualifications %>
<% } %>

<% if (!apply.equals("")) { %>
    <h2>Apply</h2>
    <%=apply %>
<% } %>

<% if (!commonPolicy.equals("")) { %>
    <h2>Diversity and Inclusion</h2>
    <%=commonPolicy %>
<% } %>