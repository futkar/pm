<%@ page
session="false"
import="org.apache.commons.lang3.StringEscapeUtils,
        com.day.cq.wcm.api.WCMMode,
        com.day.cq.wcm.api.components.DropTarget,
        com.day.cq.wcm.api.PageFilter,
        java.util.Iterator,
        java.util.List,
        java.util.ArrayList,
        org.apache.sling.jcr.resource.JcrPropertyMap
        " %><%@
include file="/apps/pennmutual/public/global.jsp"%><%@
taglib  prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%

WCMMode mode = WCMMode.fromRequest(request);

String[] pagePaths = currentStyle.get("pages", String[].class);

if (pagePaths != null && pagePaths.length > 0) {
%>
    <ul>
<%
ArrayList<Page> pages = new ArrayList<Page>(pagePaths.length);
for (String path: pagePaths) {
    Page aPage = pageManager.getPage(path);
    if (aPage == null) continue;
    pages.add(aPage);
}
    for (Page aPage: pages) {
        
        if (aPage == null) continue;
        
        String title = "";
        String cssClass = "";
        String path = aPage.getPath();
        int depth = aPage.getDepth();
        
        if (aPage.getNavigationTitle() != null) title = aPage.getNavigationTitle();
        if (title.trim().isEmpty()) title = aPage.getTitle();
        
        title = StringEscapeUtils.escapeHtml4(title);
        
        if (path.equals(currentPage.getPath())) {
            cssClass = " class=\"active\"";
        } else {
            String currentPagePath = currentPage.getPath();
            String bestPathMatch = currentPagePath;
            
            Page parent = currentPage.getParent();
            int maxDepth = 0;
            while (parent != null) {
                for (Page bPage: pages) {
                    int bPageDepth = bPage.getDepth();
                    String bPagePath = bPage.getPath();
                    if (currentPagePath.startsWith(bPagePath) && bPageDepth > maxDepth) {
                        maxDepth = bPageDepth;
                        bestPathMatch = bPagePath;
                    }
                }
                parent = parent.getParent();
            }
            if (depth == maxDepth && path.equals(bestPathMatch)){
                cssClass = " class=\"active\"";
            }
        }
        

        String windowTarget = "";

        // Final destination is first set to any redirect that exists
        Object destination = aPage.getProperties().get("redirectTarget");
        
        if(destination != null) {
            // If redirecting, open a new window
            windowTarget = "_blank";
        } else {
            // Else open in same window
            windowTarget = "";
            if (!(path.contains("://") || path.contains("mailto:"))){
                destination = path + ".html";
            } else {
                destination = path;
            }
        }
        
%>
        <li<%=cssClass %>>
            <a href="<%=destination%>" target="<%=windowTarget %>" title="<%= StringEscapeUtils.escapeHtml4(title) %>" onclick="CQ_Analytics.record({event: 'listItemClicked', values: { listItemPath: '<%=page %>' }, collect:  false, options: { obj: this }, componentPath: '<%=resource.getResourceType()%>'})"><%=title %></a>
        </li>
<%
    }
%>
    </ul>
<%
} else {
    if (WCMMode.fromRequest(slingRequest) == WCMMode.EDIT){
        %><p>No global links defined</p><%
    }
}
%>