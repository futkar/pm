<%@page import="
                com.day.cq.wcm.api.WCMMode,
                com.day.cq.wcm.api.PageFilter,
                com.pennmutual.utilities.PageUtilities,
                java.util.Iterator
                "%><%@
include file="/apps/pennmutual/public/global.jsp"%><%
WCMMode mode = WCMMode.fromRequest(request);

Page root = currentPage.getAbsoluteParent(2);
PageFilter filter = new PageFilter();
Iterator<Page> sections = root.listChildren(filter);
%>
    <div class="sitemap wrapper columns">
        <h4 class="off-screen">Penn Mutual Sitemap</h4>
        <%
        while (sections.hasNext()) {
            Page section = sections.next();
            String sectionTitle = PageUtilities.GetPageTitle(section);
            String sectionPath = section.getPath() + ".html";
            Iterator<Page> subpages = section.listChildren(filter);
        %>
            <div class="column">
                <h5><a href="<%=sectionPath%>"><%=sectionTitle %></a></h5>
                <%
                if (subpages.hasNext()) {
                %>
                <ul>
                    <%
                    while (subpages.hasNext()) {
                        Page subpage = subpages.next();
                        String subpageTitle = PageUtilities.GetNavigationTitle(subpage);
                        String subpagePath = subpage.getPath() + ".html";
                    %>
                    <li><a href="<%=subpagePath %>"><%=subpageTitle %></a></li>
                    <%
                    }
                    %>
                </ul>
                <%
                }
                %>
            </div>
        <%
        }
        %>
    </div>