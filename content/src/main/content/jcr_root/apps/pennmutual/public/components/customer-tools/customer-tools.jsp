<%@
page import="
            com.day.cq.wcm.api.WCMMode,
            com.day.cq.wcm.api.components.IncludeOptions,
            com.pennmutual.utilities.InheritanceChecker"%><%@
include file="/apps/pennmutual/public/global.jsp"%><%
WCMMode mode = WCMMode.fromRequest(request);
boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;

InheritanceChecker ic = new InheritanceChecker(currentPage);

String designPath = currentDesign.getPath();

boolean show_cs = ic.getValue("customertools/customerservice_show") == 1;
boolean show_rc = ic.getValue("customertools/repcontact_show") == 1;
boolean show_fr = ic.getValue("customertools/findarep_show") == 1;
%>
<% if (show_cs) { %>
<div class="assist customerservicebox">
    <h2><img src="<%=designPath %>/images/icon-telephone.png" alt="" width="17" height="16" />Customer Service</h2>
    <% (new IncludeOptions()).getOptions(request, true).getCssClassNames().add("off-screen"); %>
    <cq:include path="customerservice_text" resourceType="pennmutual/public/components/text" />
</div>
<% } %>
<% if (show_rc) { %>
<div class="assist contactrepbox">
    <h2><img src="<%=designPath %>/images/icon-speech-bubble.png" alt="" width="17" height="16" />Have an Advisor<br/>Contact You</h2>
    <% (new IncludeOptions()).getOptions(request, true).getCssClassNames().add("off-screen"); %>
    <cq:include path="contactreppar" resourceType="foundation/components/parsys" />
</div>
<% } %>
<% if (show_fr) { %>
<div class="assist findarepbox">
    <h2><img src="<%=designPath %>/images/icon-representative.png" alt="" width="17" height="18" />Find a Representative</h2>
    <cq:include script="find-a-rep" />
    <%--cq:include path="findareppar" resourceType="foundation/components/parsys" /--%>
</div>
<% } %>
<% if ((editing || designing) && !show_fr && !show_rc && !show_cs) { %>
<div class="cq-editrollover-insert-container">
    <span class=" cq-editrollover-insert-message cq-static">All Customer Tools disabled</span>
</div>
<% } else { %>
<script type="text/javascript">
$PML("#right-sidebar>div.customer-tools").CustomerTools();
</script>
<noscript>
    <style type="text/css">
        .customer-tools div.assist>h2~.off-screen {
            position: static !important;
            left: 0 !important;
            top: 0 !important;
        }
    </style>
</noscript>
<% } %>