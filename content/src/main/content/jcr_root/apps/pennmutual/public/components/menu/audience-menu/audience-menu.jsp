<%@ page
session="false"
import="org.apache.commons.lang3.StringEscapeUtils,
        com.day.cq.wcm.api.WCMMode,
        com.day.cq.wcm.api.components.DropTarget,
        com.day.cq.wcm.api.PageFilter,
        java.util.Iterator,
        java.util.ArrayList" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%@
taglib  prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%

WCMMode mode = WCMMode.fromRequest(request);

if (mode == WCMMode.EDIT) {
    //drop target css class = dd prefix + name of the drop target in the edit config
    String ddClassName = DropTarget.CSS_CLASS_PREFIX + "pages";
    %><div class="<%= ddClassName %>"><%
}

String[] pagePaths = currentStyle.get("pages", String[].class);

if (pagePaths != null && pagePaths.length > 0) {
	ArrayList<Page> chain = new ArrayList<Page>();
	Page parent = currentPage;
	while (parent != null) {
		  chain.add(parent);
		  parent = parent.getParent();
	}
%>
    <ul>
<%
	for (String path: pagePaths) {
		Page aPage = pageManager.getPage(path);
		if (aPage == null) continue;
		
		String title = "";
        String cssClass = "";
        
        //if (aPage.getNavigationTitle() != null) title = aPage.getNavigationTitle();
        //if (title.trim().isEmpty())
        title = aPage.getTitle();
        
        title = StringEscapeUtils.escapeHtml4(title);
        
        if (chain.indexOf(aPage) != -1 || path.equals(currentPage.getPath())) cssClass = " class=\"active\"";
%>
        <li<%=cssClass %>>
            <a href="<%=path%>.html" title="<%= StringEscapeUtils.escapeHtml4(title) %>" onclick="CQ_Analytics.record({event: 'listItemClicked', values: { listItemPath: '<%=page %>' }, collect:  false, options: { obj: this }, componentPath: '<%=resource.getResourceType()%>'})"><%=title %></a>
        </li>
<%
	}
%>
    </ul>
<%
} else {
    if (WCMMode.fromRequest(slingRequest) == WCMMode.EDIT){
        %><ul><li><a href="#">No audiences defined</a></li></ul><%
    }
}
if (mode == WCMMode.EDIT) {
%></div><%
}
%>