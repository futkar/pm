<%@
page    import="com.pennmutual.utilities.PageUtilities" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

if (request.getParameter("al") != null) {
    String destination = request.getParameter("al");
    response.sendRedirect(destination);
}
%>
        <div class="account-login">
            <p><label for="account-login">Account Login/Register</label></p>
            
            <form action="<%=PageUtilities.GetUrl(currentPage) %>" method="GET">
                <select id="account-login" name="al">
                    <option value="">Select an account</option>
                    <option value="/content/public/individuals-families/customer-service/secure.html">Client Service Center</option>
                    <option value="https://www.pennmutual.com/pmlwebsite/private/home.action">Producers Place</option>
                    <option value="https://www3483.ntrs.com/nta/pen/public/login/SignOn.jsp">Beneficiary Account</option>
                </select>
                <label class="off-screen" for="account-login-submit">Go to selected option:</label>
                <input class="off-screen" id="account-login-submit" type="submit" value="Go" />
                <%--p><a class="link-1" href="/content/public/individuals-families/performance-and-rates.html">Client Service Center</a></p>
                <p><a class="link-2" href="https://www.pennmutual.com/pmlwebsite/private/home.action">Producers Place</a></p>
                <p><a class="link-3" href="https://www3483.ntrs.com/nta/pen/public/login/SignOn.jsp">Beneficiary Account</a></p--%>
            </form>
        </div>
        <noscript>
            <style type="text/css">
                .account-login>form>#account-login-submit {
                    position: static !important;
                    left: 0 !important;
                    top: 0 !important;
                }
            </style>
        </noscript>