package com.pennmutual.ingestion;

import javax.jcr.Node;
import java.util.ArrayList;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.wcm.api.Page;
import com.day.cq.dam.indd.PageComponent;
import javax.jcr.NodeIterator;

public class FieldOfficesIngestor extends DataIngestor implements Ingestor{
	
	public FieldOfficesIngestor(){}
	
	public void extractData(WorkflowData wfData){
		try{
			log.info("\n\n****\nFieldOfficesIngestor()\n****\n\n");
			
			Node srcNode = (Node) this.session.getItem(wfData.getPayload().toString());
			ArrayList<ArrayList<String>> contents = createArrayLists(srcNode.getProperty("jcr:content/jcr:data").getString());
			
			storeData(this.datastoreNode, contents);
		} catch (Exception e){
			log.error("Error during data extraction", e);
		}
	}
	
	public ArrayList<ArrayList<String>> createArrayLists(String data){
		ArrayList<ArrayList<String>> contents = new ArrayList<ArrayList<String>>();
		
		String[] lines = data.split("\n");
		for(String line : lines){
			ArrayList<String> lineContents = new ArrayList<String>();
			String[] entries = line.split("\t");
			for(String entry : entries){
				lineContents.add(entry);
			}
			contents.add(lineContents);
		}
		return contents;
	}	
	
	public void storeData(Node datastore, ArrayList<ArrayList<String>> contents){
		
		ArrayList<String> fieldNames = contents.get(0);
		
		try{
			// Start at 2 to skip field names and "States" header;
			
			//String output = "\n\n****\n";
			
			for(int i = 2; i < contents.size(); i++){
				if(contents.get(i).size() == fieldNames.size()){
					Node stateNode = JcrUtil.createPath(datastore, beautifyName(contents.get(i).get(0)), false, "nt:unstructured", "nt:unstructured", this.session, true);
					Node repNode = JcrUtil.createPath(stateNode, beautifyName(contents.get(i).get(1)), false, "nt:unstructured", "nt:unstructured", this.session, true);
					for(int j = 0; j < contents.get(0).size(); j++){
						if(!fieldNames.get(j).equals("")){
							Node fieldNode = JcrUtil.createPath(repNode, beautifyName(fieldNames.get(j)), false, "nt:unstructured", "nt:unstructured", this.session, true);
							fieldNode.setProperty("fieldName", fieldNames.get(j));
							
							String fieldValue = contents.get(i).get(j);
							
							if (fieldValue.startsWith("\"") && fieldValue.endsWith("\"")) {
								fieldValue = fieldValue.substring(1, fieldValue.length() - 1);
								fieldValue = fieldValue.replaceAll("\"\"", "\"");
							}
							fieldNode.setProperty("fieldValue", fieldValue);
							
							/*if (fieldNode.getName().equals("regional-office")) {
								output += contents.get(i).get(j) + "\n";
							}*/
							this.session.save();
						}
					}
				} else {
					log.warn("Fields not the same size" + "\nRow: " + i + "\nSize: " + contents.get(i).size());
				}
			}
			
			//output += "\n****\n\n";
			//log.info(output);
			
		} catch (Exception e){
			log.error("Error while storing data",e);
		}
		
	}
	
	public void createPage(String path){
		
		try{
			
			Page pageRoot = this.pageManager.getPage(path);
			
			if(pageRoot == null){
				this.builder.createPage(path, "field-offices", "Field Offices", "/apps/pennmutual/public/templates/field-offices", "/etc/designs/pennmutual/public", new ArrayList<PageComponent>());
			}
			updateComponents(this.datastoreNode, pageRoot);
		} catch (Exception e ){
			log.warn("Error creating page: ", e);
		}
		
	}
	
	public void updateComponents(Node datastoreNode, Page root){
		
		ArrayList<String> componentPaths = new ArrayList<String>();
		
		try{
			String componentPath = "jcr:content/par/field-offices";
			Node pageNode = this.resourceResolver.resolve(root.getPath()).adaptTo(Node.class);
			Node fieldOfficesComponent = pageNode.getNode(componentPath);
			
			NodeIterator stateNodes = datastoreNode.getNodes();
			while(stateNodes.hasNext()){
				Node stateNode = stateNodes.nextNode();
				NodeIterator repNodes = stateNode.getNodes();
				
				while(repNodes.hasNext()){
					
					Node repNode = repNodes.nextNode();
					String officeComponentPath = "/" + stateNode.getName() + "-" + repNode.getName();
					String pathName = componentPath + officeComponentPath;
					Node officeNode = JcrUtil.createPath(pageNode, pathName, false, "nt:unstructured", "nt:unstructured", this.session, true);
					NodeIterator fieldNodes = repNode.getNodes();
					
					officeNode.setProperty("sling:resourceType", "pennmutual/public/components/ingested-data/field-offices/office");
					
					while(fieldNodes.hasNext()){
						
						Node fieldNode = fieldNodes.nextNode();
						officeNode.setProperty(fieldNode.getName(), fieldNode.getProperty("fieldValue").getString());
					}
					
					if(!officeNode.getParent().hasProperty("sling:resourceType")){
						
						officeNode.getParent().setProperty("sling:resourceType", "pennmutual/public/components/ingested-data/field-offices");
					}
					
					componentPaths.add(officeComponentPath);
					
					this.session.save();
				}
			}
			
			String[] paths = new String[componentPaths.size()];
			for(int i = 0; i < componentPaths.size(); i++){
				paths[i] = componentPaths.get(i);
			}
			
			fieldOfficesComponent.setProperty("paths", paths);
			fieldOfficesComponent.getSession().save();
			this.session.save();
			
		} catch (Exception e){
			log.error("Error updating components", e);
		}
		
	}

}
