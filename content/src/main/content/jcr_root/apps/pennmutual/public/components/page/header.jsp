<%@
page    session="false"
        import="com.day.cq.commons.Doctype,
                com.pennmutual.utilities.PageUtilities" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%@
taglib  prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %>

<div class="header" id="header">
    <div class="wrapper">
        <a href="/content/public/individuals-families.html"><img id="pml-logo" src="/content/dam/pennmutual/images/logos/logo.png" alt="Penn Mutual" height="59" width="176" /></a>
        <p class="off-screen">Global site links:</p>
        
        <cq:include path="global-links" resourceType="pennmutual/public/components/menu/global-links" />
        <!--cq:include script="global-links.jsp" /-->

        <cq:include script="search.jsp" />
        
        <cq:include script="account-login.jsp" />

        <cq:include path="audience-menu" resourceType="pennmutual/public/components/menu/audience-menu" />

    </div>
</div>