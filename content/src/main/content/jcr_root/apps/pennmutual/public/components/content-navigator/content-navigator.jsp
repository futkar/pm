<%@
page import="
            com.day.cq.wcm.api.WCMMode,
            com.day.cq.wcm.api.PageFilter,
            java.util.Iterator,
            java.util.regex.Pattern,
            java.util.regex.Matcher,
            org.apache.sling.commons.json.JSONObject,
            org.apache.sling.commons.json.JSONArray,
            com.pennmutual.utilities.PageUtilities"%><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode = WCMMode.fromRequest(request);

boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;
boolean disabled = mode == WCMMode.DISABLED;

String type = properties.get("type", "");
String headingSelector = properties.get("headingSelector", "div.heading");
String filterAction = properties.get("filterAction", "");
String id = PageUtilities.GenerateID("navigator-");
boolean deferItemAction = properties.get("deferItemAction", false);
%>
<% if (type.isEmpty()) { %>
    <p>No content navigator type set.</p>
<% } else {
    String selectLabel = "";

    if (type.equals("filter")) selectLabel = "Filter";
    else selectLabel = "Go to:";
    
    boolean includeNonItem = properties.get("includeNonItem", false);
    String nonItemCaption = properties.get("nonItemCaption", "Please select one...");
    String buttonCaption = properties.get("buttonCaption", "Go");
    String selectWidth = properties.get("selectWidth", "").trim();
    
    if (!selectWidth.isEmpty()) {
    	if (selectWidth.equals("max")) {
    		selectWidth = " style=\"max-width:100%\"";
    	} else {
    		selectWidth = " style=\"width:" + selectWidth + "\"";
    	}
    }
%>
    <div id="<%=id %>">
        <label class="off-screen" for="<%=id %>-select"><%=selectLabel %></label>
        <select id="<%=id %>-select"<%=selectWidth %>>
            <% if (includeNonItem) { %><option value=""><%=nonItemCaption %></option><% } %>
            <%
            if (type.equals("linker")) {
            %><%@include file="linker.jsp" %><%
            
            } else if (type.equals("dailyunitvalues")) {
            %><%@include file="dailyunitvalues.jsp"%><%
            
            } else if (type.equals("currentinterestrates")) {
            %><%@include file="currentinterestrates.jsp"%><%
                
            } else if (type.equals("relationship")) {
            %><%@include file="relationship.jsp" %><%
            
            }
            %>
        </select>
        <%
            if (!buttonCaption.isEmpty() && (!type.equals("filter") || deferItemAction)) { %><div class="button float"><a href="#"><%=buttonCaption %></a></div><% } %>
    </div>
    <% if (type.equals("filter")) { %>
        <script type="text/javascript">;(function($) { $(document).ready(function() { $("#<%=id%>").parent().ContentFilter({ jsonPath: '<%=resource.getPath() + ".filter.json" %>' }); }) })($PML);</script>
    <% } else { %>
    <script type="text/javascript">
    ;(function($) {
        var $nav = $('#<%=id%>');
        var $sel = $("select", $nav);
        var $btn = $(".button>a", $nav);
        $sel
            .on('change', function(e) {
                var $opt = $(':selected', $sel);
                var url = $opt.attr("value");
                var newWindow = $opt.hasClass("new-window");
                
                $btn.attr("href", url);
                   
                if (newWindow) {
                    $btn.attr("target", "_blank");
                } else {
                    $btn.attr("target", null);
                }
            });
    })($PML);
    </script><% } %>
<% } %>