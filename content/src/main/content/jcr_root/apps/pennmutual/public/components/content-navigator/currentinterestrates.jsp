<%
String currentInterestRatesPath = properties.get("currentInterestRatesPath", "");
Page cirPage = currentInterestRatesPath.isEmpty() ? currentPage : pageManager.getPage(currentInterestRatesPath);
String cirErrorMsg = "No error";
boolean cirError = true;

if (cirPage != null) {
	String cirPageUrl = PageUtilities.GetUrl(cirPage);
	Resource contentResource = cirPage.getContentResource("par");

	if (contentResource != null) {
	   Node contentNode = contentResource.adaptTo(Node.class);
	   if (contentNode != null) {
		   NodeIterator cirTables = contentNode.getNodes("manual_table*");
		   if (cirTables != null && cirTables.getSize() > 0) {
			   while(cirTables.hasNext()){ 
		            Node cirTable = cirTables.nextNode();
		            String cirSource = cirTable.hasProperty("text") ? cirTable.getProperty("text").getString() : "";
		            String cirTitle = "";
		            
		            Pattern p = Pattern.compile("<caption>(.*?)</caption>");
		            Matcher m = p.matcher(cirSource);
		            
		            while (m.find()) {
		                cirTitle = m.group(1);
		            }
		            
		            if (cirTitle.isEmpty()) continue;
		            
		            %><option value="<%=cirPageUrl%>#<%=PageUtilities.BeautifyName(cirTitle)%>"><%=cirTitle %></option><%
		        }
		       cirError = false;
		   } else {
			   cirErrorMsg = "No interest rate tables found.";
		   }
	   } else {
		   cirErrorMsg = "Couldn't resolve the node for the page's content resource.";
	   }
	} else {
		cirErrorMsg = "Content resource for page is null";
	}
} else {
    cirErrorMsg = "Couldn't find the current interest rates page (null).";
}
if ((editing || designing) && cirError) {
%><option>Error: <%=cirErrorMsg %></option><%	
}
%>