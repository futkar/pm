<%@
page    import="com.day.cq.wcm.api.WCMMode,
                java.util.Iterator,
                java.util.regex.Pattern,
                java.util.regex.Matcher,
                java.util.ArrayList,
                com.pennmutual.utilities.PageUtilities" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode =  WCMMode.fromRequest(request);
boolean isEdit = mode == WCMMode.EDIT;
boolean isDesign = mode == WCMMode.DESIGN;

String selectID = PageUtilities.GenerateID("select");
String submitID = PageUtilities.GenerateID("submit");

String ratesPath = slingRequest.getAttribute("ratesPath") != null ? (String)slingRequest.getAttribute("ratesPath") : "";

if (ratesPath.isEmpty()) return;

Page ratesPage = resourceResolver.resolve(ratesPath).adaptTo(Page.class);

if (ratesPage == null) return;

String ratesUrl = PageUtilities.GetUrl(ratesPage);

Resource contentResource = ratesPage.getContentResource("par");

if (contentResource == null) return;

Iterator<Resource> componentResources = contentResource.listChildren();

if (componentResources == null || !componentResources.hasNext()) return;

ArrayList<ArrayList<String>> links = new ArrayList();

int counter = 0;
while (componentResources.hasNext()) {
    Resource componentResource = componentResources.next();
    
    if (!componentResource.isResourceType("pennmutual/public/components/manual-table")) continue;
    
    Node node = componentResource.adaptTo(Node.class);
    
    String source = node.hasProperty("text") ? node.getProperty("text").getString() : "";
    
    String title = "";
    
    Pattern p = Pattern.compile("<caption>(.*?)</caption>");
    Matcher m = p.matcher(source);
    
    while (m.find()) {
        title = m.group(1);
    }
    
    if (title.isEmpty()) continue;
    
    ArrayList<String> entry = new ArrayList(2);
    entry.add(title);
    entry.add(ratesUrl + "#" + PageUtilities.BeautifyName(title));
    
    links.add(entry);
}

if (links.size() == 0) return;

%>
<h3><span>Rates</span></h3>
<form action="#" method="GET">
    <label class="off-screen" for="<%=selectID %>">Select a Product: </label>
    <select id="<%=selectID %>">
           <option value="">Select a product</option>
        <%
        for (ArrayList entry : links) {
        	String title = (String)entry.get(0);
        	String link = (String)entry.get(1);
        %>
           <option value="<%=link%>"><%=title %></option>
        <% } %>
    </select><br />
    <label for="<%=submitID %>" class="off-screen">View selected product:</label>
    <input id="<%=submitID %>" class="button" type="submit" value="View" />
</form>