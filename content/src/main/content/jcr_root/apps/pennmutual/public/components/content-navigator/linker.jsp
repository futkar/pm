<%

String[] pages = properties.get("pages", String[].class);

for (String aPage: pages) {
    JSONObject jsonObj = new JSONObject(aPage);
    String text = jsonObj.getString("text");
    String url = jsonObj.getString("url");
    boolean newWindow = jsonObj.getBoolean("openInNewWindow");
    String optCssClass = newWindow ? " class=\"new-window\"" : "";
    if (!url.contains(":")) {
        if (text.isEmpty()) {
            Page dest = pageManager.getPage(url);
            text = PageUtilities.GetPageTitle(dest);
        }
        url += ".html";
    }
    
%>
<option<%=optCssClass %> value="<%=url%>"><%=text %></option>
<%
}
%>