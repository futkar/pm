<%--
  Copyright 1997-2010 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Default head script.

  Draws the HTML head with some default content:
  - includes the WCML init script
  - includes the head libs script
  - includes the favicons
  - sets the HTML title
  - sets some meta data

  ==============================================================================

--%><%@include file="/apps/pennmutual/public/global.jsp" %><%
%><%@ page import="com.day.cq.commons.Doctype,
                   org.apache.commons.lang3.StringEscapeUtils" %><%
                   
    String favIcon = currentDesign.getPath() + "/favicon.ico";
    if (resourceResolver.getResource(favIcon) == null) {
        favIcon = null;
    }
    
    String windowTitle = currentPage.getProperties().get("windowTitle", currentPage.getTitle());
    
    if (windowTitle == null) windowTitle = currentPage.getName();
    
    windowTitle = StringEscapeUtils.escapeHtml4(windowTitle);
    
%><head>
    <title><%=windowTitle%></title>
    <meta charset="utf-8" />
    <meta http-equiv="keywords" content="<%= StringEscapeUtils.escapeHtml4(WCMUtils.getKeywords(currentPage, false)) %>" />
    <meta http-equiv="description" content="<%= StringEscapeUtils.escapeHtml4(properties.get("jcr:description", "")) %>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <cq:includeClientLib categories="pml.public.css" />

    <cq:includeClientLib categories="pml.public.js" />
    
    <cq:include script="/apps/pennmutual/public/init.jsp" />

    <%--cq:include script="stats.jsp" /--%>
    
    <% if (favIcon != null) { %>
    <link rel="icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>" />
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="<%= favIcon %>" />
    <% } %>

    <script type="text/javascript">
        window.pmlCookies = {};
        var rawCookies = document.cookie.split("; ");
        for(var i=0;i < rawCookies.length;i++) {
            var c = rawCookies[i].split("=");
            pmlCookies[c[0]] = c[1];
        }
    </script>
    <!-- Maxymiser script start -->
    <script type="text/javascript" src="//service.maxymiser.net/cdn/pennmutual/js/mmcore.js"></script>
    <!-- Maxymiser script end -->
</head>