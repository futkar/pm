package com.pennmutual.ingestion;

import org.apache.sling.api.resource.ResourceResolverFactory;
import com.day.cq.dam.indd.PageBuilderFactory;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.felix.scr.annotations.*;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.sling.commons.osgi.OsgiUtil;
import org.osgi.service.component.ComponentContext;
import javax.jcr.RepositoryException;
import java.util.HashMap;
import com.day.cq.wcm.api.PageManagerFactory; 

import com.day.cq.replication.*;

@Component(metatype=true, immediate=true, label="Data Ingestor Configuration", description="ingestor")
@Service
@Properties({
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Takes data from source files and creates related data-nodes in CRX"),
        @Property(name ="process.label", value = "Penn Mutual Data Ingestion"),
        @Property(name="datastore.paths", label="Datastore Paths", value={"fieldoffices.txt~/content/ingested-data/fieldoffices",""}),
        @Property(name="pagestore.paths", label="Created Pages Paths", value={"fieldoffices.txt~/content/ingested-data/fieldoffices",""}),
        @Property(name="classname.mappings", label="Class Mappings for files", value={"fieldoffices.txt~com.pennmutual.ingestion.TxtIngestor", ""})
})
public class DataIngestionProcess implements WorkflowProcess {

    private static final Logger log = LoggerFactory.getLogger(DataIngestionProcess.class);
    
    private static String[] datastoreMappings = null;
    private static String[] pagestoreMappings = null;
    private static String[] classMappings = null;
    private static HashMap<String, String> datastoreMap = null;
    private static HashMap<String, String> pagestoreMap = null;
    private static HashMap<String, String> classMap = null;
    
    
    @Reference 
    ResourceResolverFactory resolverFactory; 
    @Reference
    PageBuilderFactory builderFactory;
    @Reference
    PageManagerFactory pageManagerFactory;
    @Reference
    Replicator replicator;
    
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaData)
            throws WorkflowException {
        
        String payloadPath = workItem.getWorkflowData().getPayload().toString();
        
        // We don't want this to run on any renditions other than original.
        if(payloadPath.contains("original") || payloadPath.contains("unitval")){
            
            log.info("\n\n****\nRunning Data Ingestion Process on original rendition\n****\n\n");
            
            for(String file : classMap.keySet()){
                if(payloadPath.contains(file)){
                    try {
                        //log.info("\n\n****\n" + pagestoreMap.get(file) + "\n****\n\n");
                        Class<?> c = Class.forName(classMap.get(file));
                        DataIngestor ingestor = (DataIngestor) c.newInstance();
                        ingestor.init(resolverFactory, replicator, builderFactory, pageManagerFactory, workflowSession, pagestoreMap.get(file), datastoreMap.get(file));
                        ingestor.execute(workItem);
                    } catch (ClassNotFoundException e) {
                        log.error("Class not found, check OSGI configuration and that package has been built / deployed: ",e);
                    } catch (InstantiationException e) {
                        log.error("Instantiation exception, check that class has proper constructors: ",e);
                    } catch (IllegalAccessException e) {
                        log.error("Illegal Access Exception: ",e);
                    }
                }
            }
        } 
    }
    
    /**
     * Activates this service using a ComponentContext
     * @param context ComponentContext to utilize
     * @throws RepositoryException
     */
    protected void activate(final ComponentContext context)
            throws RepositoryException {
        datastoreMappings = OsgiUtil.toStringArray(context.getProperties().get("datastore.paths"));
        pagestoreMappings = OsgiUtil.toStringArray(context.getProperties().get("pagestore.paths"));
        classMappings = OsgiUtil.toStringArray(context.getProperties().get("classname.mappings"));
        datastoreMap = parseMappings(datastoreMappings);
        pagestoreMap = parseMappings(pagestoreMappings);
        classMap = parseMappings(classMappings);
    }

    /**
     * Sets buMappings variable to null
     * @param context ComponentContext to utilize
     */
    protected void deactivate(final ComponentContext context) {
        datastoreMappings = null;
        pagestoreMappings = null;
        datastoreMap = null;
        pagestoreMap = null;
    }
    
    private HashMap<String, String> parseMappings(String[] mappedItems){
        HashMap<String, String> returnMap = new HashMap<String, String>();
        for(String mapping : mappedItems){
            String[] mappedStore = mapping.split("~");
            if(mappedStore.length > 1){
                returnMap.put(mappedStore[0], mappedStore[1]);
            }
        }
        return returnMap;
    }

}
