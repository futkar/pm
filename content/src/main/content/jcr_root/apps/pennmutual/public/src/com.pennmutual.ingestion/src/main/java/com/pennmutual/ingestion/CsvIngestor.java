package com.pennmutual.ingestion;

import com.day.cq.workflow.exec.WorkflowData;
import java.io.InputStream;
import com.day.text.csv.Csv;
import javax.jcr.Node;
import java.util.Iterator;
import com.day.cq.commons.jcr.JcrUtil;
import java.util.ArrayList;
import com.day.cq.wcm.api.Page;
import com.day.cq.dam.indd.PageComponent;

public class CsvIngestor extends DataIngestor implements Ingestor {
	
	public CsvIngestor(){};
	
	public void extractData(WorkflowData dataItem){
		
		/*
		 * First 4 rows tied to Fund
		 * Index 0 and 1 of columns related to Code and Fund-Name respectively
		 * Codes start at Row index 4
		 */
		
		try{
			Node dataNode = (Node) this.session.getItem(dataItem.getPayload().toString());
			InputStream ips = dataNode.getProperty("jcr:content/jcr:data").getBinary().getStream();
	
			try{
				Csv csv = new Csv();
				Iterator<String[]> csvIterator = csv.read(ips, null);
				ArrayList<ArrayList<String>> csvFileContents = convertCsvToArray(csvIterator);
				for(int i = 2; i < csvFileContents.get(0).size(); i++){
					storeData(this.datastoreNode, csvFileContents, i);
				}
				csv.close();
			} catch (Exception e ){
				
			}
			ips.close();
		} catch (Exception e){ 
			log.info("Data extraction failed",e); 
		}
		
	}
	
	public ArrayList<ArrayList<String>> convertCsvToArray(Iterator<String[]> iter){
		
		ArrayList<ArrayList<String>> content = new ArrayList<ArrayList<String>>();
		 
		while(iter.hasNext()){
		
			ArrayList<String> colValues = new ArrayList<String>();
			for(String colValue : iter.next()){
				colValues.add(colValue);
			}
			content.add(colValues);
		}
		return content;
	}
	
	private void storeData(Node datastore, ArrayList<ArrayList<String>> csvContent, int index){
		
		String[] productNames = getProductNames(csvContent);
		String product = productNames[index];
		
		try{
			
			Node productNode =JcrUtil.createPath(datastore, JcrUtil.createValidName(product), false, "nt:unstructured", "nt:unstructured", this.session, true);
			productNode.setProperty("product", product);

			for(int i = 4; i < csvContent.size(); i++){

				Node codeNode = JcrUtil.createPath(productNode, csvContent.get(i).get(0), false, "nt:unstructured", "nt:unstructured", this.session, true);
				codeNode.setProperty("fundName", csvContent.get(i).get(1));
				codeNode.setProperty("fundValue", csvContent.get(i).get(index));
			}
			
			this.session.save();
		} catch (Exception e){
			
			log.warn("Error storing data", e);
		}
	}
	
	public String[] getProductNames(ArrayList<ArrayList<String>> csvContents){
		
		int numCols = csvContents.get(0).size();
		String[] productNames = new String[numCols];
		
		for(int i = 0; i < numCols; i++){
			productNames[i] = csvContents.get(0).get(i) + " " +
				csvContents.get(1).get(i) + " " +
				csvContents.get(2).get(i) + " " +
				csvContents.get(3).get(i);
		}

		return productNames;
	}
	
	
	public void createPage(String path){
		try{
			
			Page pageRoot = this.pageManager.getPage(path);
			if(pageRoot == null){
				this.builder.createPage(path, "daily-units", "Daily Units", "/apps/pennmutual/public/templates/daily-unit-values", "/etc/designs/pennmutual/public", new ArrayList<PageComponent>());
				// implement updateComponents method
			} else {
				// updateComponents method
			}
			
		} catch (Exception e ){
			log.warn("Error creating page.  Shoot.", e);
		}
	}

}
