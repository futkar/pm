<%@page session="false"
            contentType="text/html; charset=utf-8"
            import="java.util.ArrayList,
                    com.day.cq.commons.Doctype,
                    com.day.cq.wcm.api.WCMMode,
                    com.day.cq.wcm.foundation.ELEvaluator" %><%@
include file="/apps/pennmutual/public/global.jsp"
%><%
%><%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%
%><cq:defineObjects/><%
    
    WCMMode mode =  WCMMode.fromRequest(request);
    boolean isPreview = mode == WCMMode.PREVIEW;
    boolean isDisabled = mode == WCMMode.DISABLED;
    String htmlCssClass = "";
    ArrayList<String> htmlCssClasses = new ArrayList<String>();
    
    // read the redirect target from the 'page properties' and perform the
    // redirect if WCM is disabled.
    String location = properties.get("redirectTarget", "");
    // resolve variables in path
    location = ELEvaluator.evaluate(location, slingRequest, pageContext);
    if ((WCMMode.fromRequest(request) != WCMMode.EDIT && WCMMode.fromRequest(request) != WCMMode.DESIGN) && location.length() > 0) {
        // check for recursion
        if (!location.equals(currentPage.getPath())) {
            if (!(location.contains("://") || location.contains("mailto:") || location.contains("/content/dam"))) location += ".html";
            final String redirectTo = slingRequest.getResourceResolver().map(request, location);
            response.sendRedirect(redirectTo);
        } else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
        return;
    }

    
    if (currentPage.getProperties().get("cq:template", "").contains("blank-page")) {
        htmlCssClasses.add("blank-page");
    }
    
    if (htmlCssClasses.size() > 0) {
        StringBuilder sb = new StringBuilder();
        for (String s : htmlCssClasses)
        {
            sb.append(s);
            sb.append(" ");
        }
        htmlCssClass= " class=\"" + sb.toString().trim() + "\"";
    }
%>
<!DOCTYPE html>
<html<%=htmlCssClass%>>
    <cq:include script="head.jsp"/>
    <cq:include script="body.jsp"/>
    <% if (isPreview || isDisabled) { %><script type="text/javascript">;(function($) { $("#header,#main,.footer").ExternalLinkCatcher(); } )($PML);</script><% } %>
</html>