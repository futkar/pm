<%@ page import="com.day.cq.wcm.api.WCMMode,
                 com.day.cq.wcm.api.components.DropTarget,
                 com.day.cq.wcm.api.WCMMode,
                 com.day.cq.dam.api.Asset,
                 com.day.cq.dam.api.Rendition,
                 com.day.cq.wcm.foundation.Image,
                 com.day.cq.wcm.foundation.Download,
                 com.day.cq.commons.ImageHelper,
                 com.day.image.Layer,
                 com.day.cq.xss.ProtectionContext,
                 com.day.cq.xss.XSSProtectionService,
                 com.day.cq.xss.XSSProtectionException,
                 com.day.text.Text,
                 java.util.Map,
                 java.util.List,
                 java.text.DecimalFormat,
                 org.apache.commons.lang3.StringEscapeUtils" %><%
%><%@include file="/apps/pennmutual/public/global.jsp"%><%
    //drop target css class = dd prefix + name of the drop target in the edit config
    String ddClassName = DropTarget.CSS_CLASS_PREFIX + "file";

    Download dld = new Download(resource);
    if (dld.hasContent()) {
        dld.addCssClass(ddClassName);
        String title = dld.getTitle(true);
        String href = Text.escape(dld.getHref(), '%', true);
        String filename = dld.getFileName();
        String displayText = dld.getInnerHtml() == null ? filename : dld.getInnerHtml().toString();
        String description = dld.getDescription();
        
        boolean showFileSize = dld.get("showFilesize") == "true";
        boolean showFilename = dld.get("showFilename") == "true";
        boolean showThumbnail = dld.get("showThumbnail") == "true";
        boolean newWindow = dld.get("newWindow") == "true";
        
        Rendition dldRendition = null;
        
        int thumbnailWidth = 0;
        int thumbnailHeight = 0;
        
        boolean noTitle = title.trim().isEmpty();
        boolean noDescription = description.trim().isEmpty();
        
        if (!noTitle) displayText = title;
        
        String fileSize = "";
        
        if (showFileSize) {
            Property data = dld.getData();
            fileSize = HumanReadableByteCount(data.getLength(), true).toLowerCase();
        }

        if (showThumbnail) {
            Asset downloadAsset = null;
            Resource downloadResource = null;
            String fileSource = dld.getFileReference();
            
            if (fileSource.isEmpty()) fileSource = dld.getFileNodePath();
            
            downloadResource = resourceResolver.getResource(fileSource);
            downloadAsset = downloadResource.adaptTo(Asset.class);
            
            if (downloadAsset != null) {
                List<Rendition> renditions = downloadAsset.getRenditions();

                for (Rendition rendition: renditions) {
                    if (Text.getName(rendition.getPath()).startsWith("cq5dam.thumbnail.140.100.png")) {
                        dldRendition = rendition;
                        break;
                    }
                }
            }
            if (dldRendition != null) {
                Resource renditionResource = resourceResolver.getResource(dldRendition.getPath());
                if (renditionResource != null) {
                    //Image thumbnailImage = new Image(renditionResource);
                    ImageHelper imageHelper = new ImageHelper();
                    Layer layer = ImageHelper.createLayer(renditionResource);
                    if (layer != null) {
                        thumbnailWidth = layer.getWidth();
                        thumbnailHeight = layer.getHeight();
                    } else
                        showThumbnail = false;
                } else 
                    showThumbnail = false;
            } else
                showThumbnail = false;
        }
        
        XSSProtectionService xss = sling.getService(XSSProtectionService.class);
        if (xss != null) {
            try {
                displayText = xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT, displayText);
            } catch (XSSProtectionException e) {
                log.warn("Unable to protect link display text from XSS: {}", displayText);
            }
            try {
                description = xss.protectForContext(ProtectionContext.PLAIN_HTML_CONTENT, description);
            } catch (XSSProtectionException e) {
                log.warn("Unable to protect link description from XSS: {}", description);
            }
        }

        %><p class="<% if (!showThumbnail) { %>icon-<%= dld.getIconType() %><% } else { %>thumbnail<% } %>"<% if (showThumbnail) { %> style="background-image:url('<%=dldRendition.getPath() %>?1234');padding-left:<%=(thumbnailWidth + 10)%>px;min-height:<%=thumbnailHeight%>px"<% } %>>
            <a<% if (newWindow) { %> target="_blank"<% } %> href="<%= href%>" title="<%=StringEscapeUtils.escapeHtml4(title)%>" 
               onclick="CQ_Analytics.record({event: 'startDownload', values: { downloadLink: '<%=href%>' }, collect:  false, options: { obj: this, defaultLinkType: 'd' }, componentPath: '<%=resource.getResourceType()%>'})"<%
                Map<String,String> attrs = dld.getAttributes();
            if ( attrs!= null) {
                for (Map.Entry e : attrs.entrySet()) {
                    out.print(StringEscapeUtils.escapeHtml4(e.getKey().toString()));
                    out.print("=\"");
                    out.print(StringEscapeUtils.escapeHtml4(e.getValue().toString()));
                    out.print("\"");
                }
            }%>
        ><%= StringEscapeUtils.escapeHtml4(displayText) %></a><% if (showFileSize || (!noTitle && showFilename)) { %>
        (<% if (!noTitle && showFilename) { %><%=filename %><% if (showFileSize) { %>, <% } } if (showFileSize) { %><%=fileSize %><% } %>)<% }
        if (!noDescription) { %><br /><%= StringEscapeUtils.escapeHtml4(description) %><% } %></p><%
        
    } else if (WCMMode.fromRequest(request) == WCMMode.EDIT) {
        %><img src="/libs/cq/ui/resources/0.gif" class="cq-file-placeholder <%= ddClassName %>" alt=""><%
    }
%><%!

/* Thanks to stackoverflow for both of these! */

/* http://stackoverflow.com/a/3758880 */
public static String HumanReadableByteCount(long bytes, boolean si) {
    int unit = si ? 1000 : 1024;
    if (bytes < unit) return bytes + " B";
    int exp = (int) (Math.log(bytes) / Math.log(unit));
    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
}

/* http://stackoverflow.com/a/5599842 */
public String ReadableFileSize(long size) {
    if(size <= 0) return "0";
    final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
}
%>