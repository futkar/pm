<%@ page
session="false"
import="com.day.cq.wcm.api.WCMMode"
%><%@include file="/apps/pennmutual/public/global.jsp"%><%

String designPath = currentDesign.getPath();
WCMMode mode =  WCMMode.fromRequest(request);
boolean isPreview = mode == WCMMode.PREVIEW;
boolean isDisabled = mode == WCMMode.DISABLED;

%>
<div class="page-utilities">
    <ul>
        <li class="text-size"><a href="#"><img src="<%=designPath %>/images/icon-text-size.png" alt="Change Text Size" width="20" height="16" /></a></li>
        <% if (!isPreview && !isDisabled) { %>
            <li><a href="#"><img src="<%=designPath%>/images/icon-print.png" alt="Print" width="16" height="16" /></a></li>
            <li><a href="#"><img src="<%=designPath %>/images/icon-rss.png" alt="RSS feed" width="16" height="16" /></a></li>
        <% } else { %>
            <li>
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                    <a class="addthis_button_print"></a>
                    <a class="addthis_button_compact"></a>
                </div>
                <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
                <script type="text/javascript">var addthis_brand = "Penn Mutual";</script>
                <script type="text/javascript">var addthis_header_color = "#ffffff"; var addthis_header_background = "#00ACA2";</script>
                <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js#pubid=ra-5092dc262cba0570"></script>
                <!-- AddThis Button END -->
            </li>
        <% } %>
    </ul>
</div>