<%@
page    import="com.day.cq.wcm.api.WCMMode,
                org.apache.sling.commons.json.JSONObject,
                org.apache.sling.commons.json.JSONArray,
                java.util.List,
                java.util.Map,
                java.util.HashMap,
                com.pennmutual.assesstool.AssessToolUtilities,
                com.pennmutual.utilities.PageUtilities,
                com.day.cq.wcm.foundation.forms.ValidationInfo,
                com.day.jcr.vault.util.Text,
                java.net.URLEncoder"
        contentType="text/html;charset=UTF-8"%><%@
include file="/apps/pennmutual/public/global.jsp" %><%@
taglib  prefix="sling"
        uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects/>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%


final WCMMode mode = WCMMode.fromRequest(slingRequest);
final String[] quesAnsArray = properties.get("items", String[].class); // set of questions

// In the unlikely event that > 1 form is on the same page
final String [] pathNames = currentNode.getPath().split("/");
final String classId = pathNames[pathNames.length-1].replaceAll("_","");
final String baseName = pathNames[pathNames.length-1].replaceAll("(?:_\\d*)$","");

// Dialog
final String submitBtnTxt = properties.get("submitBtnTxt",String.class);
final String pathToResultsPage = properties.get("pathToResultsPage",String.class);
final String[] selectors = slingRequest.getRequestPathInfo().getSelectors();
        
//final String selectorString = slingRequest.getRequestPathInfo().getSelectorString();

//final String processPath = currentNode.getPath() + curren

StringBuffer answersString = new StringBuffer("[");

for (String selector : selectors) {
    //log("==== selector: "+selector);
    if (selector.startsWith("answers=")) {
        answersString.append(selector.replaceAll("answers=","").replaceAll( "(\\w)", ",$1").replaceFirst(",",""));
        //log(">>>> selector: "+selector);
        break;
    }

}
answersString.append("]");


boolean isEmptyJson = quesAnsArray == null || quesAnsArray.length == 0;
boolean isAuthor = mode == WCMMode.EDIT || mode == WCMMode.PREVIEW || mode == WCMMode.DESIGN;
pageContext.setAttribute("quesAnsArray",quesAnsArray);
pageContext.setAttribute("isEmptyJson",isEmptyJson);
pageContext.setAttribute("isAuthor",isAuthor);
pageContext.setAttribute("classId",classId);
pageContext.setAttribute("baseName",baseName);
pageContext.setAttribute("componentPath",currentNode.getPath());
pageContext.setAttribute("submitBtnTxt",submitBtnTxt==null || submitBtnTxt.equals("") ? "Submit" : submitBtnTxt);
pageContext.setAttribute("pathToResultsPage",
    pathToResultsPage==null || pathToResultsPage.equals(currentNode.getPath()) ? "" : pathToResultsPage);
pageContext.setAttribute("answersString",answersString);
pageContext.setAttribute("numAnswers",answersString.length()-2);

%>
<%-- --%>
<% log.info("===> answersString: "+answersString); %>
<c:if test="${numAnswers == 0}">
    
    <%@ include file="renderquestions_inc.jsp" %>
   
</c:if>
<c:if test="${numAnswers > 0}">
   
    <%@ include file="renderanswers_inc.jsp" %>
   
</c:if>