package com.pennmutual.ingestion;

import com.day.cq.workflow.exec.WorkflowData;
import java.io.InputStream;
//import org.apache.commons.sanselan.util.IoUtils;

//import com.day.text.csv.Csv;
import javax.jcr.Node;

import com.day.cq.commons.jcr.JcrUtil;
import java.util.ArrayList;
import java.util.TreeMap;
import java.io.StringWriter;

import com.day.cq.replication.*;

//Not used since disabling page creation
//import com.day.cq.wcm.api.Page;
//import com.day.cq.dam.indd.PageComponent;

public class DailyUnitValuesIngestor extends DataIngestor implements Ingestor {
	
	public DailyUnitValuesIngestor(){};
	
	public void extractData(WorkflowData dataItem){
		
		/*
		 * First 4 rows tied to Fund
		 * Index 0 and 1 of columns related to Code and Fund-Name respectively
		 * Codes start at Row index 4
		 */
		
		log.info("\n\n****\nDailyUnitValuesIngestor()\n****\n\n");
		
		try{
			Node dataNode = (Node) this.session.getItem(dataItem.getPayload().toString());
			InputStream ips = dataNode.getProperty("jcr:content/jcr:data").getBinary().getStream();
	
			datastoreNode.getNode("jcr:content").setProperty("jcr:lastModified", dataNode.getProperty("jcr:content/jcr:lastModified").getDate());
			
			//String sizes = "";
			try{
				StringWriter writer = new StringWriter();
				org.apache.commons.io.IOUtils.copy(ips, writer, null);
				String data = writer.toString();
				ips.close();
				
				String[] rowData = data.split("\n");
				
				//log.info("\n\n****\nNum rows = " + rowData.length + "\n****\n");
				
				//String sizes = "";
				
				ArrayList<ArrayList<String>> csvFileContents = convertCsvToArray(rowData);
				
				for(int i = 2; i < csvFileContents.get(0).size(); i++){
					//sizes += csvFileContents.get(i).size() + " ";
					storeData(this.datastoreNode, csvFileContents, i);
				}
				//log.info("\n\n****\nSizes: " + sizes + "\n****\n");
				
				
				//replicate the page
				log.info("\n\n****\nReplicator: " + this.replicator.toString() + "\n\nReplicating...");
				replicator.replicate(this.session, ReplicationActionType.ACTIVATE, this.datastorePath); 
				log.info("\n...done\n****\n\n");
				
			} catch (Exception e ){
				
			}
			//ips.close();
		} catch (Exception e){ 
			log.info("Data extraction failed",e); 
		}
		
	}
	
	public ArrayList<ArrayList<String>> convertCsvToArray(String[] rowData){
		
		//log.info("\n\n****\nconvertCsvToArray()\n****\n");
		
		ArrayList<ArrayList<String>> content = new ArrayList<ArrayList<String>>();
		
		for (String row: rowData) {
			String[] colValues = row.split(",");
			ArrayList<String> columnData = new ArrayList<String>();
			for (String col: colValues) {
				columnData.add(col.replaceAll("\"", ""));
			}
			content.add(columnData);
		}
		
		//log.info("\n****\nFINISHING IN convertCsvToArray()\n****\n");
		
		return content;
	}
	
	private void storeData(Node datastore, ArrayList<ArrayList<String>> csvContent, int index){
		
		String[] productNames = getProductNames(csvContent);
		String product = productNames[index];
		
		try{
			
			Node productNode =JcrUtil.createPath(datastore, JcrUtil.createValidName(product), true, "nt:unstructured", "nt:unstructured", this.session, true);
			productNode.setProperty("product", product);
			
			TreeMap<String, Integer> fundNames = new TreeMap<String, Integer>();
			
			for (int i = 4; i < csvContent.size(); i++) {
				fundNames.put(csvContent.get(i).get(1), i);
			}
			for (String fundName: fundNames.keySet()) {
				int i = fundNames.get(fundName);
				
				if (csvContent.get(i).get(index) == null || csvContent.get(i).get(index).trim().isEmpty()) continue;
				
				Node codeNode = JcrUtil.createPath(productNode, csvContent.get(i).get(0), true, "nt:unstructured", "nt:unstructured", this.session, true);
				codeNode.setProperty("fundName", csvContent.get(i).get(1));
				codeNode.setProperty("fundValue", csvContent.get(i).get(index));
			}
			/*for (int i = 4; i < csvContent.size(); i++){
				
				//Skip items that have no value.
				if (csvContent.get(i).get(index) == null || csvContent.get(i).get(index).trim().isEmpty()) continue;
				
				Node codeNode = JcrUtil.createPath(productNode, csvContent.get(i).get(0), true, "nt:unstructured", "nt:unstructured", this.session, true);
				codeNode.setProperty("fundName", csvContent.get(i).get(1));
				codeNode.setProperty("fundValue", csvContent.get(i).get(index));
			}*/
			
			this.session.save();
		} catch (Exception e){
			
			log.warn("Error storing data", e);
		}
	}
	
	public String[] getProductNames(ArrayList<ArrayList<String>> csvContents){
		
		int numCols = csvContents.get(0).size();
		String[] productNames = new String[numCols];
		
		for(int i = 0; i < numCols; i++){
			productNames[i] = csvContents.get(0).get(i) + " " +
				csvContents.get(1).get(i) + " " +
				csvContents.get(2).get(i) + " " +
				csvContents.get(3).get(i);
		}

		return productNames;
	}
	
	
	public void createPage(String path){
	}

}
