<%@
page    session="false"
        import="com.day.cq.commons.Doctype"
%><%@
include file="/apps/pennmutual/public/global.jsp"
%><%@
taglib  prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"
%>
	<div class="main-menu" id="main-menu">
	   <p class="off-screen">Main menu for [the current audience]</p>
    
	    <ul class="wrapper">
	        <li><a href="#">Home</a></li>
	        <li><a href="#">Your Financial Life</a></li>
	        <li><a href="#">Life Insurance</a></li>
	        <li><a href="#">Annuities</a></li>
	        <li><a href="#">Performance &amp; Rates</a></li>
	        <li><a href="#">Customer Service</a></li>
	    </ul>
	    <script type="text/javascript" >
	    ;(function($) {
	        PML.MainMenu($);
	    })($PML);
	    </script>
	</div>