<%@
page session="false" %><%@
page import="com.day.cq.wcm.api.WCMMode,
             com.day.cq.wcm.api.components.IncludeOptions" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode = WCMMode.fromRequest(request);
boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;

IncludeOptions ic = (new IncludeOptions()).getOptions(request, true);

//componentContext.setDecorate(false);
%>
<cq:include path="listitempar" resourceType="pennmutual/public/components/list/parsys" />