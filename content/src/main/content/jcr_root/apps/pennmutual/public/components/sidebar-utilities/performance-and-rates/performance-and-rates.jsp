<%@
page    import="com.day.cq.wcm.api.WCMMode,
                com.pennmutual.utilities.PageUtilities" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode =  WCMMode.fromRequest(request);
boolean isEdit = mode == WCMMode.EDIT;
boolean isDesign = mode == WCMMode.DESIGN;

String id = PageUtilities.GenerateID("p-and-r");
String performancePath = properties.get("performancePath", "");
String ratesPath = properties.get("ratesPath", "");
boolean nothingSet = performancePath.isEmpty() && ratesPath.isEmpty();
%>
<%
if (!nothingSet) {
    if (!performancePath.isEmpty()) {
        slingRequest.setAttribute("performancePath", performancePath);
    %><cq:include script="performance.jsp" /><%
    }
    if (!ratesPath.isEmpty()) {
        slingRequest.setAttribute("ratesPath", ratesPath);
    %><cq:include script="rates.jsp" /><%
    }
%>   
    <script id="<%=id %>" type="text/javascript">
    ;(function($) {
        var $comp = $("script#<%=id %>").parent("div");
        
        if (!$comp || !$comp.length) return;
        
        $comp
            .addClass("off-screen")
            .children("form")
                .on('submit', function() {
                    var $form = $(this);
                    var $selectedOption = $("select>option:selected", $form);
                    var value = $selectedOption.val();
                    
                    if (!value) return false;
                    
                    if ($.browser.msie && $.browser.version < 9) {
                        location.href = value;
                        return false;
                    }
                    
                    $form.attr("action", value);

                    return true;
                })
                .children("select")
                    .on('change', function() {
                        var $select = $(this),
                            disabled = !$("option:selected", $select).val(),
                            $submit = $select.nextAll(":submit");

                        // Disabled the disabled attribute for now.
                        /*if (disabled) $submit.attr("disabled", "disabled");
                        else $submit.removeAttr("disabled");*/
                    })
                    .change();
                
        $comp
            .addClass("dynamic");
     
        var $headers = $("h3", $comp);
        var $previousHeader = null;
        var offset = 0;
        $headers
            .each(function(idx) {
	            if (idx > 0) { 
	                $(this).css("left", offset);
	            }
	            offset += $(this).outerWidth(true);
	            $previousHeader = $(this);
	        })
	        .on('click', function() {
	           $(this)
	               .siblings($(this).get(0).tagName)
	                   .removeClass("active")
	                   .end()
	               .addClass("active")
	               .next("form")
	                   .siblings("form")
	                       .hide()
	                       .end()
	                   .show();
	        });
        
        $comp
            .removeClass("off-screen");
        
    })($PML || $);
    </script>
<% } else if (isEdit || isDesign) { %>
    <p>No performances and rates paths set.</p>
<% } %>