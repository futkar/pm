package com.pennmutual.ingestion;

import com.day.cq.dam.indd.PageBuilderFactory;
import org.apache.sling.api.resource.ResourceResolverFactory;
import com.day.cq.wcm.api.PageManagerFactory;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.replication.*;

public interface Ingestor {
	
	void init(ResourceResolverFactory resolverFactory, Replicator replicator, PageBuilderFactory pageFactory, PageManagerFactory managerFactory, WorkflowSession wfSession, String pageStore, String dataStore);
	
	void execute(WorkItem workItem);
	
	void extractData(WorkflowData wfData); 
	
	void createDataStore(String dataStorePath);
	
	void createPage(String path);
	
}
