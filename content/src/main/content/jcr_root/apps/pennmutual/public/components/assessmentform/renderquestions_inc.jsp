<%
    // getQuestionList(final String[] quesAnsArray)
    List<String> questionList = AssessToolUtilities.getQuestionList(quesAnsArray);
    pageContext.setAttribute("questionList", questionList);
%>

<div class="assessment-ques-wrapper">
    <c:if test="${empty questionList}">
        <p>Warning:<br />
        No questions have been provided. Please provide the assessment questions by editing the component.</p>
    </c:if>

<c:if test="${!empty questionList}">
    <div class="assessment-form-wrapper">
    <form id="${classId}-form" name="${classId}-form" action="" method="get">
    
    <div>
<c:forEach items="${questionList}" var="question" varStatus="status">
        <div class="assessment-ques-row">
            <div class="assessment-ques-num">${status.index+1}</div>
            <div class="assessment-ques-area">
                ${question}
                
                <div class="assessment-btn-wrapper">
                    <c:set var="radioName" value="${classId}-rb-${status.index+1}"/>
          
                    <input name="${radioName}" class="assessment-yesbtn radio" type="radio" value="yes" /> <span class="assessment-btn-lbl">Yes</span>
                    <input name="${radioName}" class="assessment-nobtn radio" type="radio" value="no" /> <span class="assessment-btn-lbl">No</span>
                    <span class="assessment-ques-msg"></span>
                </div>
            </div>
        </div>
            <div class="clear"></div>
       
</c:forEach>
    </div>


    <input id="${classId}-submit" class="ui-button ui-widget ui-state-default ui-corner-all" type="submit" value="${submitBtnTxt}" >
    
    
    </form>
    <div class="error-msg"></div>
    </div>
</c:if>

<script type="text/javascript">
;(function($) {
    var submitId = "${classId}-submit";
    var formId = "${classId}-form";
    var classId = "${classId}";
    var currentPage = "${currentPage.path}";
   //$(document).ready(function() {
    $('.assessment-ques-wrapper').ready(function() {
       $("#"+formId).submit(function(obj) {
           var isValid = true;
           var answers = "";
           $('#'+formId+' .assessment-ques-row').each(function(index, elem) {
               var nchecked = $(this).find("input:checked[type='radio']").length;
               answers += $(this).find("input:checked[type='radio']").val()=='yes' ? '1' : '0';                                    
               if (nchecked == 0) {
                   //$(this).addClass("error-notchecked");
                  //$(this).parent('.assessment-ques-row').addClass("error-notchecked");
                   $(this).addClass("error-notchecked");
                   $(this).find('span.assessment-ques-msg').text('Input is required');
                   isValid = false;
               }
           });
           //answers = isValid ? answers : "";
           $('#'+formId).attr('action',currentPage+'.answers='+answers+'.html');
          //$('#'+formId).attr('action',currentPage);
           return isValid;
        });
        
        $('#'+formId+' .assessment-ques-row :radio').each(function(index,elem) {
            $(this).click(function(obj) {
                $(this).parents('.assessment-ques-row').removeClass("error-notchecked");
                $(this).siblings('span.assessment-ques-msg').text('');
            });
        });
    });
})($PML);
</script>
</div>