<%@
include file="/apps/pennmutual/public/global.jsp" %><%@
page    import="
                java.util.ArrayList,
                com.day.cq.wcm.api.WCMMode,
                org.apache.sling.api.request.RequestPathInfo,
                com.pennmutual.utilities.PageUtilities"
%><%
String  srcPath =   properties.get("path", "/content/ingested-data/fieldoffices");
Node    node =      (Node) resourceResolver.resolve(srcPath).adaptTo(Node.class);
String  selectedState = slingRequest.getParameter("state");

if (selectedState == null) selectedState = "";
else selectedState = selectedState.trim().toLowerCase();

try {
%>
<ul<% if (!selectedState.isEmpty()) { %> class="state-<%=selectedState%>"<% } %>>
<%
    NodeIterator states = node.getNodes();

    while (states.hasNext()) {
        Node state = states.nextNode();
        NodeIterator offices = state.getNodes();
        
        while (offices.hasNext()) {
        	Node office = offices.nextNode();
%>
    <li<% if (selectedState.equals(state.getName())) { %> style="display:list-item"<% } %>>
        <span class="key" style="display:none"><%=state.getName().toUpperCase() %></span>
        <cq:include path="<%=office.getPath() %>" resourceType="<%=component.getPath() + "/office"%>" />
    </li>
<%
        }
    }
%>
</ul>
<cq:includeClientLib categories="pml.public.states-data" />
<noscript>
    <style type="text/css">
        div.field-offices ul li {
            display: block;
        }
    </style>
</noscript>
<%
} catch (Exception e) {
	out.write("<li>NO DATA</li>");
}
%>