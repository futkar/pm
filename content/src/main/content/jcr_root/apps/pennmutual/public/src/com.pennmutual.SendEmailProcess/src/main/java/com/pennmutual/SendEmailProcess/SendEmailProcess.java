package com.pennmutual.SendEmailProcess;

import java.io.InputStream;
import java.io.StringWriter;

import com.day.cq.dam.indd.PageBuilderFactory;

import com.day.cq.mailer.MailService;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;

import org.apache.commons.io.IOUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.ResourceResolverFactory;

import org.osgi.framework.Constants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

//import org.apache.sling.commons.osgi.OsgiUtil;

import org.osgi.service.component.ComponentContext;

@Component(metatype=true, immediate=true, label="Penn Mutual Send Email Configuration", description="SendEmailProcess")
@Service
@Properties({
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Takes payload and template path and sends emails"),
        @Property(name ="process.label", value = "Penn Mutual Send Email Process")
})
public class SendEmailProcess implements WorkflowProcess {

    private static final Logger log = LoggerFactory.getLogger(SendEmailProcess.class);
    
    @Reference
    MailService mailService;
    @Reference 
    ResourceResolverFactory resolverFactory; 
    @Reference
    PageBuilderFactory builderFactory;
    
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaData)
            throws WorkflowException {
        
        log.info("\n\n****\nRunning the Penn Mutual Send Email Process\n****\n\n");
        
        String templatePath = metaData.get("PROCESS_ARGS", "");
        WorkflowData workflowData = workItem.getWorkflowData();
        String payloadType = workflowData.getPayloadType();
        String payloadPath = "";
        
        if (templatePath.isEmpty()) {
            throw new WorkflowException("No path to template specified.");
        }
        
        //log.info("\n\n****\nPath to template = " + templatePath + "\n****\n\n");
        //log.info("\n\n****\nPayload type = " + workItem.getWorkflowData().getPayloadType() + "\n****\n\n");
        
        if (!payloadType.equals("JCR_PATH")) {
            throw new WorkflowException("Payload Type is not JCR_PATH");
        }
        
        payloadPath = workflowData.getPayload().toString();
        
        Node payloadNode = null;
        Session session = workflowSession.getSession();
        
        try {
            payloadNode = (Node)session.getItem(payloadPath);
        } catch(Exception exception) {
            throw new WorkflowException("Couldn't find/open the payload path", exception);
        }
        
        String templateText = "";
        
        try {
            Node templateNode = (Node)session.getItem(templatePath);
            InputStream ips = templateNode.getProperty("jcr:content/jcr:data").getBinary().getStream();
            StringWriter writer = new StringWriter();
            IOUtils.copy(ips, writer);
            templateText = writer.toString();
        } catch(Exception exception) {
            throw new WorkflowException("Couldn't find/open the template path", exception);
        }

        try {
        	SendEmail sendEmail = new SendEmail(templateText, payloadNode, mailService);
        
        	sendEmail.Send();
        
        } catch (Exception ex) {
        	throw new WorkflowException("Failed to create or send the email.", ex);
        }
    }
    
    /**
     * Activates this service using a ComponentContext
     * @param context ComponentContext to utilize
     * @throws RepositoryException
     */
    protected void activate(final ComponentContext context)
            throws RepositoryException {

    }

    /**
     * Sets buMappings variable to null
     * @param context ComponentContext to utilize
     */
    protected void deactivate(final ComponentContext context) {

    }
   

}