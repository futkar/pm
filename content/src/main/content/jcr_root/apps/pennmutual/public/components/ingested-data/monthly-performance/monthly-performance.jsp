<%@
include file="/apps/pennmutual/public/global.jsp" %><%@
page    import="
                java.util.ArrayList,
                com.day.cq.wcm.api.WCMMode,
                com.pennmutual.utilities.PageUtilities"
%><%
try {
    String              srcData     = properties.get("path", "");
    Node                srcNode     = resourceResolver.resolve(srcData).adaptTo(Node.class);
    ArrayList<String>   fieldNames  = new ArrayList<String>();
    Node                fieldNode   = srcNode.getNode("fields");
    String              asOfDate    = fieldNode.getProperty("asOfDate").getString();
    NodeIterator        fields      = fieldNode.getNodes();
    String              linkPrefix  = "/content/public/individuals-families/performance-and-rates/fund-summaries";
    WCMMode             mode        = WCMMode.fromRequest(request);
    boolean             editing     = mode == WCMMode.EDIT;
    boolean             designing   = mode == WCMMode.DESIGN;
    
    while(fields.hasNext()){
        Node nextNode = fields.nextNode();
        if(nextNode.hasProperty("fieldName")){
            fieldNames.add(nextNode.getProperty("fieldName").getString());
        }
    }
    
    
%>
<table class="style-3 monthly-performance" summary="Monthly performance data for <%=asOfDate%>">
    <thead>
        <tr>
            <td rowspan="2" colspan="2" class="empty-cell caption">
                <%=PageUtilities.GetPageTitle(currentPage) %>
            </td>
            <th class="no-bottom-border-for-row" colspan="8">
                For Period Ending <%=asOfDate %>
            </th>
        </tr>
        <tr>
            <th class="no-border-til-end no-top-border-for-row" colspan="3"><span>Percentage Change</span></th>
            <th class="last" colspan="5"><span>Average Annual Returns</span></th>
        </tr>
        <tr>
            <th scope="col" class="no-border-til-end">Fund Name</th>
            <th scope="col">Sub-Adviser/Adviser</th>
            <th scope="col">1 Mth</th>
            <th scope="col">3 Mth</th>
            <th scope="col">YTD</th>
            <th scope="col">12 Mths</th>
            <th scope="col">3 Yr</th>
            <th scope="col">5 Yr</th>
            <th scope="col">10 Yr or Since Inception &dagger;</th>
            <th scope="col" class="last">Inception Date</th>
        </tr>
    </thead>
<%
    NodeIterator categories = srcNode.getNodes();
    while(categories.hasNext()){
        Node category = categories.nextNode();
        String nodeName = category.getName();
        
        if(nodeName.equals("fields") || nodeName.equals("jcr:content")){
            continue;
        }
        String categoryName = category.getProperty("categoryName").getString();
        %>
    <thead>
       <tr><th colspan="10"><%= categoryName %></th></tr>
    </thead>
    <tbody>
        <%
        NodeIterator funds = category.getNodes();
        while(funds.hasNext()){
            Node fund = funds.nextNode();
            String fundName = fund.getProperty("fundName").getString();
            String annotation = fund.hasNode("annotation") && fund.getNode("annotation").hasProperty("fieldValue") ? fund.getNode("annotation").getProperty("fieldValue").getString() : "";
            String summaryPath = FormDetailLink(linkPrefix, categoryName, fundName);
            
            if (!annotation.isEmpty()) annotation = "<sup>" + annotation + "</sup>";
            %>
        <tr>
            <th><a href="<%=summaryPath%>"><%=fundName%></a><%=annotation %><% if ((editing || designing) && !DetailPageExists(pageManager, summaryPath)) {%><span class="required">(missing: <%=summaryPath%>)</span><% } %></th>
            <%
            NodeIterator columns = fund.getNodes();
            int counter = 9;
            while(columns.hasNext()) {
                if (counter == 0) break;
                
                Node column = columns.nextNode();
                String columnName = column.getName();
                
                if (columnName.equals("annotation") || columnName.equals("monthly_as-of")) continue;
                
                String fieldValue = "";
                if (column.hasProperty("fieldValue")) fieldValue = column.getProperty("fieldValue").getString();
                
                %>
            <td><%=fieldValue%></td>
                <%
                counter--;
            }
            %>
        </tr>
            <%
        }
        %>
    </tbody>
        <%
    }
%>
</table>
<%
    

%>

<% } catch (Exception e){
    out.write("NO DATA: " + e.getMessage());
}
%><%!
String FormDetailLink(String prefix, String categoryName, String fundName) {
    return prefix + "/" + PageUtilities.BeautifyName(categoryName) + "/" + PageUtilities.BeautifyName(fundName) + ".html";
}
boolean DetailPageExists(PageManager pageManager, String path) {
    //if (path.endsWith(".html")) path = path.substring(0, path.length() - 5);
    if (path.endsWith(".html")) path = path.replace(".html", "");
    return pageManager.getPage(path) != null;
}
%>