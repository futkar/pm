<%@ page
session="false"
import="org.apache.commons.lang3.StringEscapeUtils,
        com.day.cq.wcm.api.WCMMode,
        com.day.cq.wcm.api.components.DropTarget,
        com.day.cq.wcm.api.PageFilter,
        java.util.Iterator,
        org.apache.sling.api.resource.ValueMap,
        com.pennmutual.utilities.PageUtilities,
        java.util.ArrayList" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%@
taglib  prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %><%

int     depth = currentStyle.get("depth", 2);
Page    parent = currentPage.getAbsoluteParent(depth);

if (parent == null) return;

WCMMode         mode = WCMMode.fromRequest(request);
ValueMap        componentProperties = component.getProperties();
boolean         includeJavascript = componentProperties.get("includeJavascript", true);
boolean         includeHome = componentProperties.get("includeHome", false);
boolean         isSecondary = component.getName().equals("secondary-menu");
int             currentPageDepth = currentPage.getDepth();
ArrayList<String> chain = new ArrayList<String>(currentPageDepth);
Iterator<Page>  children = parent.listChildren(new PageFilter());
Page            pageChainCrawler = currentPage;

// Get all the pages in the path from the current to the page matching the depth of the menu.

while (pageChainCrawler != null && depth < currentPageDepth) {
	chain.add(pageChainCrawler.getPath());
	pageChainCrawler = pageChainCrawler.getParent();
	currentPageDepth--;
}

ValueMap parentProperties = parent.getProperties();
String title = "";//parentProperties.get("subtitle", parentProperties.get("navigationTitle", parent.getTitle()));

if (isSecondary) {
	title = PageUtilities.GetSubnavigationTitle(parent, true);
} else {
    title = PageUtilities.GetNavigationTitle(parent, true);
}
String cssClass = parent.getPath().equals(currentPage.getPath()) ? " class=\"active\"" : "";
String path = parent.getPath();

%>
    <p class="off-screen">Menu for the &ldquo;<%=parent.getTitle() %>&rdquo; section.</p>
    <ul class="wrapper">
        <% if (includeHome && parent.getProperties().get("redirectTarget", "").isEmpty()) { %>
        <li<%=cssClass %>>
            <a href="<%=path %>.html"><%=title %></a>
        </li>
        <% } %>
<%
    while (children.hasNext()) {
    	Page child = children.next();
    	
        if (child == null) continue;
        
        title = "";
        cssClass = "";
        path = child.getPath();
        
        if (isSecondary) {
            title = PageUtilities.GetSubnavigationTitle(child, true);
        } else {
            title = PageUtilities.GetNavigationTitle(child, true);
        }
        
        if (chain.contains(path)) cssClass = " class=\"active\"";
%>
        <li<%=cssClass %>>
            <a href="<%=path%>.html" title="<%= StringEscapeUtils.escapeHtml4(title) %>" onclick="CQ_Analytics.record({event: 'listItemClicked', values: { listItemPath: '<%=page %>' }, collect:  false, options: { obj: this }, componentPath: '<%=resource.getResourceType()%>'})"><%=title %></a>
        </li>
<%
    }
%>
    </ul>
    <% if (includeJavascript) { %>
    <script type="text/javascript" >
        ;(function($) {
            PML.MainMenu($);
        })($PML);
    </script> 
    <% } %>   