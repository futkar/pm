<%
%><%@include file="/apps/pennmutual/public/global.jsp"%><%
%><%@page session="false" %><%
%><%
String cssClass = properties.get("cssClass", "");
String href = properties.get("href", "");
int height = properties.get("height", 300);
int width = properties.get("width", 100);
String dimensions = properties.get("dimensions", "%px");

if (!cssClass.trim().isEmpty()) cssClass = " class=\"" + cssClass + "\"";

String style = "width:{w}{wuom};height:{h}{huom}";

style = style.replace("{w}", width + "");
style = style.replace("{h}", height + "");

if (dimensions.startsWith("%")) {
    style = style.replace("{wuom}", "%");
} else {
    style = style.replace("{wuom}", "px");
}
if (dimensions.endsWith("%")) {
    style = style.replace("{huom}", "%");
} else {
    style = style.replace("{huom}", "px");
}
String ua = request.getHeader("User-Agent");
boolean isMSIE = (ua != null && ua.indexOf("MSIE") != -1);
double version = 0;
if (isMSIE) {
    String tempStr = ua.substring(ua.indexOf("MSIE"),ua.length());
    version = Double.parseDouble(tempStr.substring(4,tempStr.indexOf(";")).trim());
}
%>
<iframe src="<%=href%>" frameborder="0" scrolling="auto" style="<%=style%>" <%=cssClass %>></iframe>
