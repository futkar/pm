<%@ page
import="com.pennmutual.utilities.PageUtilities" %><%@
include
file="/apps/pennmutual/public/global.jsp"%><%

boolean section = properties.get("section", false);

if (!section) {
%><h1><%=PageUtilities.GetPageTitle(currentPage) %></h1>
<%
} else {
    Page sectionPage = pageManager.getContainingPage(resource);
    if (sectionPage != null) {
%><p class="section-title"><%=PageUtilities.GetPageTitle(sectionPage) %></p><%
    }
}
%>