<%@
page import="
            com.day.cq.wcm.api.WCMMode,
            org.apache.sling.commons.json.JSONObject"%><%@
include file="/apps/pennmutual/public/global.jsp"%><%

WCMMode mode = WCMMode.fromRequest(request);

boolean editing = mode == WCMMode.EDIT;
boolean designing = mode == WCMMode.DESIGN;
boolean preview = mode == WCMMode.PREVIEW;
boolean disabled = mode == WCMMode.DISABLED;

JSONObject json = new JSONObject();

json.put("headingFilter", properties.get("headingFilter", "div.heading"));
json.put("itemSelector", properties.get("itemSelector", ""));
json.put("preFilterAction", properties.get("preFilterAction", ""));
json.put("afterFilterCreationAction", properties.get("afterFilterCreationAction", ""));
json.put("nonItemAction", properties.get("nonItemAction", ""));
json.put("itemAction", properties.get("itemAction", ""));
json.put("deferItemAction", properties.get("deferItemAction", false));
%>
<%=json.toString() %>