package com.pennmutual.assesstool;

//import java.io.IOException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class AssessToolUtilities {

    public AssessToolUtilities() {}
    

    public static String getMessage() { return new String("Hello"); } 
    
    
    public static String getScoreFromAnswers(int numYes, final String[] scoringClasses) throws JSONException {
     
        String score = null;
        if (scoringClasses != null) {
            // Example json strings
            // {"interval":"6","clsname":"no risk"}
            // {"interval":"4-5","clsname":"low risk"}
        
            for (String scoringClass : scoringClasses) {
                JSONObject jsonObj = new JSONObject(scoringClass);
                String intervalStr = jsonObj.getString("interval");
                String v1Str = intervalStr.replaceAll("\\s+","").replaceAll("-\\d+$","");
                String v2Str = intervalStr.replaceAll("\\s+","").replaceAll("^\\d+-","");
                
                int v1 = Integer.parseInt(v1Str);
                int v2 = Integer.parseInt(v2Str);
                
                if (( v1 <= numYes && numYes <= v2) || (v2 <= numYes && numYes <= v1)) {
                    score = jsonObj.getString("clsname");
                    break;
                }
            }
        } 
        return score;
    } 
    
    
    public static String applyTokens(Map<String,Object> valueMap, String inputText) {
        final String outText = (inputText == null || inputText.equals("") ? "" : inputText)
            .replaceAll("\\$\\{score\\}", (String)valueMap.get("score"))
            .replaceAll("\\$\\{numyes\\}",Integer.toString((Integer)valueMap.get("numyes")))
            .replaceAll("\\$\\{numquestions\\}",Integer.toString((Integer)valueMap.get("numquestions")))
            ;
        return outText;
    } 
    

    public static List<String> getResponseList(final JSONArray userAnswerArray, final String[] quesAnsArray) throws JSONException {   
        
        List<String> responseList = new ArrayList<String>();   
        for (int i=0;i<userAnswerArray.length();i++) {
            JSONObject jqaRow = new JSONObject(quesAnsArray[i]);
            String responseText = jqaRow.getString("response");
            
            if (userAnswerArray.getString(i).equals("0")) {
                responseList.add(responseText);
            }
        
        }
        return responseList;
    }    

    public static List<String> getQuestionList(final String[] quesAnsArray) throws JSONException {   
       // final JSONArray userAnswerArray = new JSONArray(answersString);
        List<String> questionList = new ArrayList<String>();
        
        if (quesAnsArray == null) return questionList;
        
        for (int i=0;i<quesAnsArray.length;i++) {
           // String qaRow = quesAnsArray[i];
            JSONObject jqaRow = new JSONObject(quesAnsArray[i]);
            String questionText = jqaRow.getString("question");
            questionList.add(questionText);
        }
        return questionList;
    }
}
