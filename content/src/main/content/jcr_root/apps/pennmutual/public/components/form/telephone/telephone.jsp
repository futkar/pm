<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Form 'element' component

  Draws an element of a form

--%><%@include file="/apps/pennmutual/public/global.jsp"%><%
%><%@ page import="com.day.cq.wcm.foundation.forms.FormsHelper,
        com.day.cq.wcm.foundation.forms.LayoutHelper,
        org.apache.commons.lang3.StringEscapeUtils,
        java.util.Map,
        java.util.LinkedHashMap,
        java.util.Locale,
		java.util.ResourceBundle,
		com.day.cq.i18n.I18n" %>
<%
	
	final Locale pageLocale = currentPage.getLanguage(true);
	final ResourceBundle bundle = slingRequest.getResourceBundle(pageLocale);
	I18n i18n = new I18n(bundle);
		
    final String name = FormsHelper.getParameterName(resource);
    final String id = FormsHelper.getFieldId(slingRequest, resource);
    final boolean required = FormsHelper.isRequired(resource);
    final boolean hideTitle = properties.get("hideTitle", false);
    final String title = FormsHelper.getTitle(resource, i18n.get("Telephone"));
    
    final String areacode = FormsHelper.getValue(slingRequest, resource, name + ".areacode");
    final String prefix = FormsHelper.getValue(slingRequest, resource, name + ".prefix");
    final String number = FormsHelper.getValue(slingRequest, resource, name + ".number");
    final String extension = FormsHelper.getValue(slingRequest, resource, name + ".extension");

    %>
    <div class="form_row">
        <% LayoutHelper.printTitle(id, title, required, hideTitle, out); %>
        <div class="form_leftcolnobr telephone-areacode">
             <label class="off-screen" for="<%= StringEscapeUtils.escapeHtml4(id + "-areacode") %>">Area Code</label>
             ( <input
                type="text"
                class="<%= FormsHelper.getCss(properties, "form_field form_field_text form_telephone_areacode") %>"
                id="<%= StringEscapeUtils.escapeHtml4(id + "-areacode") %>"
                name="<%= StringEscapeUtils.escapeHtml4(name + ".areacode") %>"
                value="<c:out value="<%= areacode != null ? areacode : "" %>"/>"
                size="4"> )
           
        </div>
        <div class="form_leftcolnobr telephone-prefix">
            <label class="off-screen" for="<%= StringEscapeUtils.escapeHtml4(id + "-prefix") %>">Prefix</label>
            <input
                type="text"
                class="<%= FormsHelper.getCss(properties, "form_field form_field_text form_telephone_prefix") %>"
                id="<%= StringEscapeUtils.escapeHtml4(id + "-prefix") %>"
                name="<%= StringEscapeUtils.escapeHtml4(name + ".prefix") %>"
                value="<c:out value="<%= prefix != null ? prefix : "" %>"/>"
                size="16">
        </div>
        <div class="form_leftcolnobr">&nbsp;-&nbsp;</div>
        <div class="form_leftcolnobr telephone-number">
            <label class="off-screen" for="<%= StringEscapeUtils.escapeHtml4(id + "-number") %>">Number</label>
            <input
                type="text"
                class="<%= FormsHelper.getCss(properties, "form_field form_field_text form_telephone_number") %>"
                id="<%= StringEscapeUtils.escapeHtml4(id + "-number") %>"
                name="<%= StringEscapeUtils.escapeHtml4(name + ".number") %>"
                value="<c:out value="<%= number != null ? number : "" %>"/>" >
        </div>
        <div class="form_leftcolnobr telephone-extension">
            <label for="<%= StringEscapeUtils.escapeHtml4(id + "-extension") %>">Ext.</label>
            <input
                type="text"
                class="<%= FormsHelper.getCss(properties, "form_field form_field_text form_telephone_extension") %>"
                id="<%= StringEscapeUtils.escapeHtml4(id + "-extension") %>"
                name="<%= StringEscapeUtils.escapeHtml4(name + ".extension") %>"
                value="<c:out value="<%= extension != null ? extension : "" %>"/>" >
        </div>
    </div>
    <%
    LayoutHelper.printDescription(FormsHelper.getDescription(resource, ""), out);
    LayoutHelper.printErrors(slingRequest, name, hideTitle, out);
%>
