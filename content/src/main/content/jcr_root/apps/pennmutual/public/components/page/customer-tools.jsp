<%@
page import="com.day.cq.wcm.api.WCMMode"%><%@
include file="/apps/pennmutual/public/global.jsp"
%><%

boolean hide = properties.get("customertools/hide", false);

if (!hide) {
	WCMMode mode = WCMMode.fromRequest(request);
	
	boolean editing = mode == WCMMode.EDIT;
	boolean isHomePage = currentPage.getDepth() == 2;
	
	Resource ct = null;
	
	String basePath = !isHomePage ? currentPage.getAbsoluteParent(1).getPath() : currentPage.getPath();
	
	if (!isHomePage) {
	    ct = resourceResolver.getResource(basePath + "/jcr:content/customertools");
	}
%>
<% if (isHomePage) { %>
<cq:include path="customertools" resourceType="pennmutual/public/components/customer-tools" />
<% } else if (ct != null) { %>
<sling:include resource="<%=ct%>" />
<% } else if (editing) { %>
<p>No Customer Tools component found on the root page.</p>
<% } %>
<%
}
%>