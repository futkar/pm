<%@
page session="false" %><%@
page import="com.pennmutual.utilities.PageUtilities,
             com.day.cq.wcm.api.WCMMode,
             java.util.regex.Pattern,
             java.util.regex.Matcher" %><%@
include file="/apps/pennmutual/public/global.jsp"%><%

String text = properties.get("text", "");

Pattern p = Pattern.compile("<caption>(.+?)</caption>");
Matcher m = p.matcher(text);

while (m.find()) {
    String caption = m.group(1);
    if (caption.isEmpty()) break;
    text = text.replace("<table", "<table id=\"" + PageUtilities.BeautifyName(caption) + "\"");
    break;
}

%><%=text %>