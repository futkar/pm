        <form class="search" action="/content/public/individuals-families/search.html" method="GET">
            <label class="off-screen" for="site-search">Search:</label>
            <input type="text" id="site-search" name="q" placeholder="Search" value="" />
            <label class="submit" for="submit-search">Submit Search:</label>
            <input id="submit-search" type="submit" value="Go" class="off-screen" />
        </form>