package com.pennmutual.utilities;

import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Integer;

public class DateUtilities {
	
	public static String FormatDate(String szDate, String format) {
	    String[] dateParts = szDate.split("/");
	    Calendar cal = Calendar.getInstance();
	    
	    cal.set(Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[0])-1, Integer.parseInt(dateParts[1]));
	    
	    Date date = cal.getTime();
	    
	    return FormatDate(date, format);
	}
	public static String FormatDate(Date date, String format) {
	    SimpleDateFormat sdf = new SimpleDateFormat(format);
	    
		return sdf.format(date);
	}	
	public static String FormatDate(Calendar calendar, String format) {
		//return "MM/DD/YYY";
		return FormatDate(calendar.getTime(), format);
	}
}