<%
%><%@ page session="false"
           import="java.text.DateFormat,
                   java.util.Date,
                   java.util.Calendar,
                   com.day.cq.wcm.api.Page,
                   org.apache.commons.lang3.StringEscapeUtils,
                   com.pennmutual.utilities.PageUtilities,
                   com.pennmutual.utilities.DateUtilities"%>
<%@include file="/libs/foundation/global.jsp"%><%

    Page listItem = (Page)request.getAttribute("listitem");
    Resource opportunityResource = resourceResolver.resolve(listItem.getContentResource().getPath() + "/par/opportunity");
    Node opportunityNode = opportunityResource.adaptTo(Node.class);

    String title = PageUtilities.GetPageTitle(listItem);
    String positionOverview = "";
    if (opportunityNode.hasProperty("positionOverview")) positionOverview = opportunityNode.getProperty("positionOverview").getString();
    Date defaultDate = new Date();

    Date date = opportunityNode.getProperty("postedDate").getDate().getTime();
    
    %><li><p><a href="<%= listItem.getPath() %>.html" title="<%= StringEscapeUtils.escapeHtml4(title) %>"
                onclick="CQ_Analytics.record({event: 'listNewsItemClicked', values: { listItemPath: '<%= listItem.getPath() %>' }, collect:  false, options: { obj: this }, componentPath: '<%=resource.getResourceType()%>'})"><%

    %><span class="title"><%= StringEscapeUtils.escapeHtml4(title) %></span></a><br /><%
                
    if (defaultDate.getTime() != date.getTime()) {
        %><span class="date">POSTED: <%=DateUtilities.FormatDate(date, "M/d/yyyy")%></span><%
    }
    if (!positionOverview.equals("")) {
        %><%=positionOverview%><%   
    
    }
%></p></li>