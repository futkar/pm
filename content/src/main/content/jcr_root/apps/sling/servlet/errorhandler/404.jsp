<%--
  Copyright 1997-2008 Day Management AG
  Barfuesserplatz 6, 4001 Basel, Switzerland
  All Rights Reserved.

  This software is the confidential and proprietary information of
  Day Management AG, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with Day.

  ==============================================================================

  Generic 404 error handler

  Important note:  
  Since Sling uses the user from the request (depending on the authentication
  handler but typically HTTP basic auth) to login to the repository and JCR/CRX
  will simply say "resource not found" if the user does not have a right to
  access a certain node, everything ends up in this 404 handler, both access
  denied ("401", eg. for non-logged in, anonymous users) and really-not-existing
  scenarios ("404", eg. logged in, but does not exist in repository).
  
--%><%
%><%@ page session="false" %><%
%><%@ page import="
    java.net.URLEncoder,
    org.apache.sling.api.scripting.SlingBindings,
    org.apache.sling.engine.auth.Authenticator,
    org.apache.sling.engine.auth.NoAuthenticationHandlerException,
    com.day.cq.wcm.api.WCMMode,
                    java.io.PrintWriter,
                    org.apache.sling.api.SlingConstants,
                    org.apache.sling.api.request.RequestProgressTracker,
                    org.apache.sling.api.request.ResponseUtil,
                    org.apache.commons.lang3.StringEscapeUtils" %><%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0" %><%!

    private boolean isAnonymousUser(HttpServletRequest request) {
        return request.getAuthType() == null
            || request.getRemoteUser() == null;
    }

    private boolean isBrowserRequest(HttpServletRequest request) {
        // check if user agent contains "Mozilla" or "Opera"
        final String userAgent = request.getHeader("User-Agent");
        return userAgent != null
            && (userAgent.indexOf("Mozilla") > -1
                || userAgent.indexOf("Opera") > -1);
    }
    
%><%

    // decide whether to redirect to the (wcm) login page, or to send a plain 404
    if (WCMMode.fromRequest(request) != WCMMode.DISABLED) {
        if (isAnonymousUser(request)
                && isBrowserRequest(request)) {
            
            SlingBindings bindings = (SlingBindings) request.getAttribute("org.apache.sling.api.scripting.SlingBindings");
            Authenticator auth = bindings.getSling().getService(Authenticator.class);
            if (auth != null) {
                try {
                    auth.login(request, response);
                    
                    // login has been requested, nothing more to do
                    return;
                } catch (NoAuthenticationHandlerException nahe) {
                    bindings.getLog().warn("Cannot login: No Authentication Handler is willing to authenticate");
                }
            } else {
                bindings.getLog().warn("Cannot login: Missing Authenticator service");
            }
            
        }
    }
    // get here if authentication should not take place or if
    // no Authenticator service is available or if no
    // AuthenticationHandler is willing to authenticate
    // So we fall back to plain old 404/NOT FOUND   
     
    // Set Status Code to proper 404
    response.setStatus(404);
   
    // Define site paths
    String pmam_site_filter = "/content/public/asset-management";
    String pml_site_filter_1 = "/content/public/individuals-families";
    String pml_site_filter_2 = "/content/public/small-business";
    String pml_site_filter_3 = "/content/public/financial-professionals";
    String pml_site_filter_4 = "/content/public/about-pennmutual";
    String htk_site_filter = "/content/htk";
   
    String requested = request.getRequestURI();
    String pnf_url = "";
    String pnf_node = "/page_not_found.html";
   
    if( requested.lastIndexOf(pml_site_filter_1) > -1)
    {
        pnf_url = pml_site_filter_1 + pnf_node;
    }
    else if( requested.lastIndexOf(pml_site_filter_2) > -1)
    {
        pnf_url = pml_site_filter_2 + pnf_node;
    }
    else if( requested.lastIndexOf(pml_site_filter_3) > -1)
    {
        pnf_url = pml_site_filter_3 + pnf_node;
    }
    else if( requested.lastIndexOf(pml_site_filter_4) > -1)
    {
        pnf_url = pml_site_filter_4 + pnf_node;
    }
    else if( requested.lastIndexOf(pmam_site_filter) > -1)
    {
        pnf_url = pmam_site_filter + pnf_node;
    }
    else if(requested.lastIndexOf(htk_site_filter) > -1) {
        pnf_url = htk_site_filter + pnf_node;
    }
    else { // PML is the default
        pnf_url = pml_site_filter_1 + pnf_node;
    }
    String includeURL = StringEscapeUtils.escapeHtml4(pnf_url);   
%>
<sling:include path="<%= includeURL %>"/>